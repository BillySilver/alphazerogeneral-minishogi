import numpy as np
from pytorch_classification.utils import Bar, AverageMeter
import time

class Arena():
    """
    An Arena class where any 2 agents can be pit against each other.
    """
    def __init__(self, player1, player2, game, display=None):
        """
        Input:
            player 1,2: two functions that takes board as input, return action
            game: Game object
            display: a function that takes board as input and prints it (e.g.
                     display in othello/OthelloGame). Is necessary for verbose
                     mode.

        see othello/OthelloPlayers.py for an example. See pit.py for pitting
        human players/other baselines with each other.
        """
        self.player1 = player1
        self.player2 = player2
        self.game = game
        self.display = display

    def playGame(self, verbose=False):
        """
        Executes one episode of a game.

        Returns:
            either
                winner: player who won the game (1 if player1, -1 if player2)
            or
                draw result returned from the game that is neither 1, -1, nor 0.
        """
        players = [self.player2, None, self.player1]
        curPlayer = 1
        board = self.game.getInitBoard()
        round = 0
        while self.game.getGameEnded(board, curPlayer) == 0:
            if round >= 200:
                print("Tie game. Too many moves.")
                return self.game.getGameEnded(board, 1)

            round += 1
            if verbose:
                assert(self.display)
                print("\nTurn", str(round), "Player", str(curPlayer))
                self.display(board)
            action = players[curPlayer+1](self.game.getCanonicalForm(board, curPlayer), round)

            valids = self.game.getValidMoves(self.game.getCanonicalForm(board, curPlayer), 1, round=round)

            if valids[action] == 0:
                print(action)
                assert valids[action] > 0
            board, curPlayer = self.game.getNextState(board, curPlayer, action)
        if verbose:
            assert(self.display)
            print("Game over. Player " + str(self.game.getGameEnded(board, 1)) + " is win.")
            self.display(board)

        # bookkeeping
        print('\nThis game ends with', round, 'rounds')
        return self.game.getGameEnded(board, 1)

    def playGames(self, num, verbose=False):
        """
        Plays num games in which player1 starts num/2 games and player2 starts
        num/2 games.

        Returns:
            oneWon:     games won by player1
            twoWon:     games won by player2
            draws:      games won by nobody
        """
        eps_time = AverageMeter()
        bar = Bar('Arena.playGames', max=num)
        end = time.time()
        eps = 0
        maxeps = int(num)

        num = int(num / 2)
        oneFirstWon = 0
        twoFirstWon = 0
        oneSecondWon = 0
        twoSecondWon = 0
        draws = 0

        for _ in range(num):
            gameResult = self.playGame(verbose=verbose)
            if gameResult == 1:
                oneFirstWon += 1
            elif gameResult == -1:
                twoSecondWon += 1
            else:
                draws += 1
            # bookkeeping + plot progress
            print(oneFirstWon, twoFirstWon, oneSecondWon, twoSecondWon, draws)
            eps += 1
            eps_time.update(time.time() - end)
            end = time.time()
            bar.suffix = '(%(index)d/%(max)d) Eps Time: {et:.3f}s | Total: %(elapsed_td)s | ETA: %(eta_td)s'.format(et=eps_time.avg)
            bar.next()

        self.player1, self.player2 = self.player2, self.player1

        for _ in range(num):
            gameResult = self.playGame(verbose=verbose)
            if gameResult == -1:
                oneSecondWon += 1
            elif gameResult == 1:
                twoFirstWon += 1
            else:
                draws += 1
            # bookkeeping + plot progress
            print(oneFirstWon, twoFirstWon, oneSecondWon, twoSecondWon, draws)
            eps += 1
            eps_time.update(time.time() - end)
            end = time.time()
            bar.suffix = '(%(index)d/%(max)d) Eps Time: {et:.3f}s | Total: %(elapsed_td)s | ETA: %(eta_td)s'.format(et=eps_time.avg)
            bar.next()

        bar.finish()

        return oneFirstWon, twoFirstWon, oneSecondWon, twoSecondWon, draws

    def playGame_anyfirst(self, is_player1_first, verbose=False):
        oneFirstWon = 0
        twoFirstWon = 0
        oneSecondWon = 0
        twoSecondWon = 0
        draws = 0

        if not is_player1_first:
            self.player1, self.player2 = self.player2, self.player1

        gameResult = self.playGame(verbose=verbose)
        if is_player1_first:
            if gameResult == 1:
                oneFirstWon += 1
            elif gameResult == -1:
                twoSecondWon += 1
            else:
                draws += 1
        else:
            self.player1, self.player2 = self.player2, self.player1
            if gameResult == -1:
                oneSecondWon += 1
            elif gameResult == 1:
                twoFirstWon += 1
            else:
                draws += 1

        return oneFirstWon, twoFirstWon, oneSecondWon, twoSecondWon, draws
