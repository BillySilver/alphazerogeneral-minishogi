from collections import deque
from Arena import Arena
# from Exact_Win_MCTS import MCTS
from MCTS import MCTS
import numpy as np
from pytorch_classification.utils import Bar, AverageMeter
import time
import os
import sys
from pickle import Pickler, Unpickler
from random import shuffle


class Coach():
    """
    This class executes the self-play + learning. It uses the functions defined
    in Game and NeuralNet. args are specified in main.py.
    """

    def __init__(self, game, nnet, args):
        self.game = game
        self.nnet = nnet
        self.pnet = self.nnet.__class__(self.game)  # the competitor network
        self.args = args
        self.mcts = MCTS(self.game, self.nnet, self.args)
        # history of examples from args.numItersForTrainExamplesHistory latest iterations
        self.trainExamplesHistory = []
        self.skipFirstSelfPlay = False  # can be overriden in loadTrainExamples()

    def executeEpisode(self):
        """
        This function executes one episode of self-play, starting with player 1.
        As the game is played, each turn is added as a training example to
        trainExamples. The game is played till the game ends. After the game
        ends, the outcome of the game is used to assign values to each example
        in trainExamples.
        It uses a temp=1 if episodeStep < tempThreshold, and thereafter
        uses temp=0.
        Returns:
            trainExamples: a list of examples of the form (canonicalBoard,pi,v)
                           pi is the MCTS informed policy vector, v is +1 if
                           the player eventually won the game, else -1.
        """
        trainExamples = []
        board = self.game.getInitBoard()
        self.curPlayer = 1
        episodeStep = 0

        while True:
            episodeStep += 1
            print("Round ", episodeStep)
            print("Start board: ")
            self.game.display(board, self.curPlayer)
            print()

            canonicalBoard = self.game.getCanonicalForm(board, self.curPlayer)
            temp = int(episodeStep < self.args.tempThreshold)

            pi = self.mcts.getActionProb(canonicalBoard, temp=temp, round=episodeStep)
            trainExamples.append([canonicalBoard, self.curPlayer, pi, None])

            action = np.random.choice(len(pi), p=pi)

            board, self.curPlayer = self.game.getNextState(board, self.curPlayer, action)

            r = self.game.getGameEnded(board, self.curPlayer)

            if episodeStep > 200:   # Absolutely repeat repetitions
                print("\nThe game moves = 200, so retraining.")
                return 0

            if r != 0:
                print("Final board: ")
                self.game.display(board, self.curPlayer)
                print("curPlayer =", self.curPlayer)
                print("r =", r)
                # print("\nThe game moves =", episodeStep)
                print()
                return [(x[0], x[2], r*((-1)**(x[1] != self.curPlayer))) for x in trainExamples]

    def learn(self):
        """
        Performs numIters iterations with numEps episodes of self-play in each
        iteration. After every iteration, it retrains neural network with
        examples in trainExamples (which has a maximium length of maxlenofQueue).
        It then pits the new neural network against the old one and accepts it
        only if it wins >= updateThreshold fraction of games.
        """
        import datetime     # Custom verbose for profiling.

        for i in range(len(self.trainExamplesHistory) + 1, self.args.numIters + 1):
            # bookkeeping
            print('------ITER ' + str(i) + '/' + str(self.args.numIters) + '------')
            # examples of the iteration
            if not self.skipFirstSelfPlay or i > 1:
                iterationTrainExamples = deque(
                    [], maxlen=self.args.maxlenOfQueue)

                eps_time = AverageMeter()
                bar = Bar('Self Play', max=self.args.numEps)
                end = time.time()

                for eps in range(self.args.numEps):
                    # reset search tree
                    self.mcts = MCTS(self.game, self.nnet, self.args)
                    while True:
                        executeEpisode = self.executeEpisode()
                        if executeEpisode:
                            iterationTrainExamples += executeEpisode
                            break

                    # bookkeeping + plot progress
                    eps_time.update(time.time() - end)
                    end = time.time()
                    bar.suffix = '({eps}/{maxeps}) Eps Time: {et:.3f}s | Total: {total:} | ETA: {eta:}'.format(eps=eps+1, maxeps=self.args.numEps, et=eps_time.avg,
                                                                                                               total=bar.elapsed_td, eta=bar.eta_td)
                    bar.next()
                bar.finish()

                if True == self.args.is_compressing:
                    from scipy import sparse
                    result = iterationTrainExamples
                    result = [( sparse.csr_matrix(np.vstack([
                                    np.concatenate([
                                        x[0][:, :, :20],                                        # Normal pieces on board.
                                        np.append(x[0][0, 0, 20: ], [0]*5).reshape(5, 5, 1)     # Captured pieces: 0/1. Stored in first 20 of (5, 5, 1).
                                    ], axis=-1).reshape(1, -1)
                                    for x in result ])) ,
                                np.vstack([ x[1] for x in result ]) ,
                                np.array([ x[2] for x in result ], dtype=np.int8) )]
                    iterationTrainExamples.clear()
                    iterationTrainExamples += result
                    del result
                not self.args.is_custom_verbose or print('Total steps of iteration:', len(iterationTrainExamples[0][1] if self.args.is_compressing else iterationTrainExamples))    # list[0] -> tuple[1] -> np.ndarray (if compressed).

                # save the iteration examples to the history
                self.trainExamplesHistory.append(iterationTrainExamples)
                not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', 'trainExamplesHistory extended.')

            if len(self.trainExamplesHistory) > self.args.numItersForTrainExamplesHistory:
                print("len(trainExamplesHistory) =", len(
                    self.trainExamplesHistory), " => remove the oldest trainExamples")
                self.trainExamplesHistory.pop(0)
            # backup history to a file
            # NB! the examples were collected using the model from the previous iteration, so (i-1)
            # NB! OneNet is use to now iteration train examples, so (i)
            self.saveTrainExamples(i)
            # ^^^^^^^^^^^^^^^^^^^^^^^^^ It may cost lots of time and memory! Thus excluded temporarily.
            #                           However, if a early trained model is loaded,
            #                           there will be no history examples for training.

            # shuffle examlpes before training
            not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', 'Before obtaining trainExamples.')
            trainExamples = []
            for e in self.trainExamplesHistory:
                trainExamples.extend(e)
            shuffle(trainExamples)

            if not self.args.oneNet:
                # training new network, keeping a copy of the old one
                self.nnet.save_checkpoint(
                    folder=self.args.checkpoint, filename='temp.pth.tar')
                self.pnet.load_checkpoint(
                    folder=self.args.checkpoint, filename='temp.pth.tar')
                pmcts = MCTS(self.game, self.pnet, self.args)

                self.nnet.train(trainExamples)
                nmcts = MCTS(self.game, self.nnet, self.args)

                print('PITTING AGAINST PREVIOUS VERSION')
                arena = Arena(lambda x, round: np.argmax(pmcts.getActionProb(
                    x, temp=0, round=round)), lambda x: np.argmax(nmcts.getActionProb(x, temp=0, round=round)), self.game)
                pfwins, nfwins, pswins, nswins, draws = arena.playGames(
                    self.args.arenaCompare)
                pwins = pfwins + pswins
                nwins = nfwins + nswins

                print('NEW/PREV WINS : %d / %d ; DRAWS : %d' %
                      (nwins, pwins, draws))
                if pwins+nwins > 0 and float(nwins)/(pwins+nwins) < self.args.updateThreshold:
                    print('REJECTING NEW MODEL')
                    self.nnet.load_checkpoint(
                        folder=self.args.checkpoint, filename='temp.pth.tar')
                else:
                    print('ACCEPTING NEW MODEL')
                    self.nnet.save_checkpoint(
                        folder=self.args.checkpoint, filename=self.getCheckpointFile(i))
                    self.nnet.save_checkpoint(
                        folder=self.args.checkpoint, filename='best.pth.tar')
            else:
                not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', 'Before train().')
                self.nnet.train(trainExamples)
                not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', '', end='')
                print('COMPLETING UPDATE')
                self.nnet.save_checkpoint(
                    folder=self.args.checkpoint, filename='best_' + str(i) + '.pth.tar')

    def getCheckpointFile(self, iteration):
        return 'best_' + str(iteration) + '.pth.tar'

    def saveTrainExamples(self, iteration):
        folder = self.args.checkpoint
        if not os.path.exists(folder):
            os.makedirs(folder)
        filename = os.path.join(
            folder, self.getCheckpointFile(iteration) + ".examples")
        with open(filename, "wb+") as f:
            Pickler(f).dump(self.trainExamplesHistory)
        f.closed

    def loadTrainExamples(self):
        modelFile = os.path.join(
            self.args.load_folder_file[0], self.args.load_folder_file[1])
        examplesFile = modelFile + ".examples"
        if not os.path.isfile(examplesFile):
            print(examplesFile)
            r = input("File with trainExamples not found. Continue? [y|n]")
            if r != "y":
                sys.exit()
        else:
            print("File with trainExamples found. Read it.")
            with open(examplesFile, "rb") as f:
                self.trainExamplesHistory = Unpickler(f).load()
            f.closed
            # examples based on the model were already collected (loaded)
            # self.skipFirstSelfPlay = True
