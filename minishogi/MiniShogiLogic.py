from minishogi.MiniShogiBoard import *
from minishogi.MiniShogiPieces import *
from minishogi.MiniShogiPlayer import *
import time
import os
import sys
import logging
import numpy as np


class loggingException(Exception):
    pass


class Board():
    pieceDict = {'k': 0, 'g': 1, 's': 2, 'b': 3, 'r': 4, 'p': 5, '+s': 6, '+b': 7, '+r': 8, '+p': 9,
                 'K': 10, 'G': 11, 'S': 12, 'B': 13, 'R': 14, 'P': 15, '+S': 16, '+B': 17, '+R': 18, '+P': 19,
                 'cg': 20, 'cs': 21, 'cb': 22, 'cr': 23, 'cp': 24, 'cG': 25, 'cS': 26, 'cB': 27, 'cR': 28, 'cP': 29,
                 'c2g': 30, 'c2s': 31, 'c2b': 32, 'c2r': 33, 'c2p': 34, 'c2G': 35, 'c2S': 36, 'c2B': 37, 'c2R': 38, 'c2P': 39}
    # Static variable for several functions.
    __pieceDict = {
        'k': King,   'g': GGeneral, 's': SGeneral,
        'b': Bishop, 'r': Rook,     'p': Pawn,
        'K': King,   'G': GGeneral, 'S': SGeneral,
        'B': Bishop, 'R': Rook,     'P': Pawn }
    # Static variable for getObjGameBoard().
    __pieceOrder = [King, GGeneral, SGeneral, Bishop, Rook, Pawn, SGeneral, Bishop, Rook, Pawn]
    # Static variable for getObjGameCapture().
    __strOrder = ['g', 's', 'b', 'r', 'p']
    # Static variable for init_board().
    __init_nngameboard = np.zeros((5, 5, 40), dtype=np.int8)
    for x in range(5):
        __init_nngameboard[x, 0, x] = 1
        __init_nngameboard[4-x, 4, 10+x] = -1
    __init_nngameboard[0, 1, 5] = 1
    __init_nngameboard[4, 3, 15] = -1

    def __init__(self, nngameboard: np.ndarray, player: int):
        objgameboard, list_king, list_pawn = self.__getOGB(nngameboard)
        captured_player, captured_enemy    = self.__getOGC(nngameboard, player)

        self.__objgameboard = objgameboard
        self.__list_king = list_king
        self.__list_pawn = list_pawn
        # The turn player.
        self.__obj_player = Player(player, captured_player)
        # The opposite player of obj_player.
        self.__obj_enemy = Player(-player, captured_enemy)
        # To reuse Board in MiniShogiGame, it should be compatible with
        # MiniShogiGame.getCanonicalForm().
        self.is_rotated = False

    def set_turn_player(self, player: int, to_rotate: bool = False):
        if to_rotate:
            self.is_rotated = not self.is_rotated

        if self.is_rotated ^ (self.__obj_player.team != player):
            self.__obj_player, self.__obj_enemy = self.__obj_enemy, self.__obj_player

    @classmethod
    def init_board(cls):
        return cls(cls.__init_nngameboard, player=1)

    @property
    def objgameboard(self):
        return self.__objgameboard

    @property
    def obj_player(self):
        return self.__obj_player

    @property
    def obj_enemy(self):
        return self.__obj_enemy

    def __cache_move_updater(self, piece_init, piece_dest, init_pos, dest_pos, to_promote):
        try:
            board = self.__nngb_cache
        except:
            # If nngameboard cache is not existed, create it.
            board = self.getNNGameBoard(self.__objgameboard, self.__obj_player, self.__obj_enemy)
            self.__nngb_cache = board
            return

        if piece_dest:
            k = piece_dest._piece_code
            board[dest_pos + (k, )] = 0

            k = piece_dest._pcode   # From 0 to 5.
            if k != 0:  # Capture board has no spaces for Kings.
                p_enemy = piece_dest.team
                k = k-1 + 20 + 5*(p_enemy == 1)     # 1. Capture board starts from z=20; 2. Captured ally became a foe.
                if board[0, 0, k]:
                    # c_ -> c2_. Two pieces of same type are captured.
                    k += 10
                board[:, :, k] = -p_enemy

        k = piece_init._piece_code
        player = piece_init.team
        k_promoted = 4 if to_promote else 0     # Whether the piece was unpromoted before the move.
        board[init_pos + (k-k_promoted, )] = 0
        board[dest_pos + (k, )] = player

    def __cache_drop_updater(self, str_piece, dest_pos):
        try:
            board = self.__nngb_cache
        except:
            # If nngameboard cache is not existed, create it.
            board = self.getNNGameBoard(self.__objgameboard, self.__obj_player, self.__obj_enemy)
            self.__nngb_cache = board
            return

        k = self.pieceDict['c' + str_piece]
        if board[0, 0, k+10]:
            # Have priority to pop a later capture piece in the same type.
            k += 10
        player = board[0, 0, k]
        board[:, :, k] = 0

        k = self.pieceDict[str_piece]
        board[dest_pos + (k, )] = player

    __swap_normal_piece_idx = np.roll(np.arange(20), 10)
    __swap_capture1_piece_idx = np.roll(np.arange(20, 30), 5)
    __swap_capture2_piece_idx = np.roll(np.arange(30, 40), 5)
    @property
    def nngameboard(self):
        try:
            board = self.__nngb_cache
        except:
            # If nngameboard cache is not existed, create it.
            board = self.getNNGameBoard(self.__objgameboard, self.__obj_player, self.__obj_enemy)
            self.__nngb_cache = board

        board = board.copy()    # To prevent an outside board changing.
        if self.is_rotated:
            """ Similar part from MiniShogiGame.getCanonicalForm() """
            #
            # Rotate 2 times (180 degree, counterclockwise).
            # A rotated board and original one share the same space, thus side effect
            # exists.
            board = np.rot90(board, 2)
            # Switch owners of pieces (including captured).
            board *= -1
            # Swap sub-boards of two players.
            board[:, :, :20] = board[:, :, self.__swap_normal_piece_idx]
            #
            # Emergency procedures for improper MiniShogiGame.getCanonicalForm().
            # Will be replaced if MiniShogiGame.getCanonicalForm() is fixed.
            board[:, :, 20:30] = board[:, :, self.__swap_capture1_piece_idx]
            board[:, :, 30:]   = board[:, :, self.__swap_capture2_piece_idx]
        return board

    @property
    def strgameboard(self):
        # Generate strgameboard on demand.
        return self.getStrGameBoard(self.__objgameboard)

    # Parse function for only one string coordinate
    @classmethod
    def parsePos(cls, str_pos):
        x = ord(str_pos[0]) - 97
        y = int(str_pos[1]) - 1
        return (x, y)

    # Parse instruction: return ( move type (move or drop), start pos or piece to drop, end pos, promote? )
    __dict_move_str_to_tuple = {}
    @classmethod
    def _move_parser(cls, str_move: str) -> tuple:
        try:    return cls.__dict_move_str_to_tuple[str_move]
        except: pass

        try:
            moves = str_move.split()
            dest_pos = cls.parsePos(moves[2])
            if moves[0] == 'move':
                init_pos = cls.parsePos(moves[1])
                promote = False
                if len(moves) > 3:
                    promote = True
                tuple_result = (moves[0], init_pos, dest_pos, promote)
            else:
                tuple_result = (moves[0], moves[1], dest_pos, False)
            cls.__dict_move_str_to_tuple[str_move] = tuple_result
            return tuple_result
        except ValueError:
            raise Exception('Illegal move.')

    # Parse function coordinate -> string
    @classmethod
    def parseToString(cls, coord):
        strPos = chr(coord[0] + 97)
        strPos += str(coord[1] + 1)
        return strPos

    rotateMoveDict = {}     # Dynamically built by _rotateActionMove().
    @classmethod
    def _rotateActionMove(cls, str_move: str) -> str:
        try:    return cls.rotateMoveDict[str_move]
        except: pass

        b = Board   # No instance created. Using class methods below.
        moves = str_move.split()
        dest_pos_x, dest_pos_y = b.parsePos(moves[2])
        if moves[0] == 'move':  # move
            init_pos_x, init_pos_y = b.parsePos(moves[1])
            moves[1] = b.parseToString((4-init_pos_x, 4-init_pos_y))
        else:  # drop
            moves[1] = moves[1].swapcase()
        moves[2] = b.parseToString((4-dest_pos_x, 4-dest_pos_y))
        str_rotate_move = ' '.join(moves)
        cls.rotateMoveDict[str_move] = str_rotate_move
        cls.rotateMoveDict[str_rotate_move] = str_move  # Due to symmetry.
        return str_rotate_move

    def __is_in_check(self, king_pos: tuple, obj_the_player: Player):
        """
        Arguments:
            king_pos {tuple} -- a 2-tuple to indicate where is the King.
            obj_the_player {Player} -- a custom Player object which is the owner
                                       of the King. it should be either
                                       self.obj_player or self.obj_enemy.

        Returns:
            check {bool} -- whether King is in check.
            set_enemy_moves {set} -- all positions where foe pieces can arrive.
            set_threatening_pos {set} -- all positions of threatening pieces.
            set_block_pos {set} -- all positions of grids between the King
                                   and threatening Rooks/Bishops.
        """
        objgameboard = self.__objgameboard

        check = False
        set_enemy_moves = set()
        set_threatening_pos = set()
        set_block_pos = set()
        # Loop through gameboard to find other teams' pieces and the moves associated with those pieces.
        rows, cols = objgameboard.nonzero()
        pieces = objgameboard[(rows, cols)]
        for idx in range(len(pieces)):
            foe_piece = pieces[idx]
            if foe_piece.team == obj_the_player.team:
                continue
            x = rows[idx]
            y = cols[idx]
            # Note: from foe_piece to the King.
            dx = king_pos[0] - x
            dy = king_pos[1] - y

            pieceType = foe_piece.pieceType
            if pieceType in {'Bishop', 'Rook'}:
                foe_piece_moves = set(foe_piece.moves(objgameboard))
            elif abs(dx) <= 2 and abs(dy) <= 2:     # King, GGeneral, SGeneral, Pawn.
                # Specified moves() for checking less moves.
                foe_piece_moves = foe_piece.threatening_moves(dx, dy, objgameboard)
            else:
                continue
            set_enemy_moves |= foe_piece_moves
            if king_pos not in foe_piece_moves:
                continue
            check = True
            # Moving to this position (where the threatening piece is) can
            # prevent King from being in check. That is, capturing the foe piece.
            set_threatening_pos.add((x, y))

            # Bishops and Rooks have the special ability to move more than one space away from current position
            # -> test for ways to block the check.
            if pieceType not in {'Bishop', 'Rook'}:
                continue
            n_grid = abs(dx or dy)      # Ignore 0. The Rook/Bishop needs to cross over n_grid grids to take our King out.
            dx, dy = dx//n_grid, dy//n_grid
            for i in range(1, n_grid):
                set_block_pos.add((x + i*dx, y + i*dy))

        return check, set_enemy_moves, set_threatening_pos, set_block_pos

    def __is_in_check_lite(self, king_pos: tuple, obj_the_player: Player) -> bool:
        """Lite version of __is_in_check()
        Arguments:
            king_pos {tuple} -- a 2-tuple to indicate where the King is.
            obj_the_player {Player} -- a custom Player object which is the owner
                                       of the King. it should be either
                                       self.obj_player or self.obj_enemy.

        Returns:
            {bool} -- whether King is in check.
        """
        # Loop through gameboard to find other teams' pieces and the moves associated with those pieces.
        objgameboard = self.__objgameboard

        rows, cols = objgameboard.nonzero()
        pieces = objgameboard[(rows, cols)]
        for idx in range(len(pieces)):
            foe_piece = pieces[idx]
            if foe_piece.team == obj_the_player.team:
                continue
            # Note: from foe_piece to the King.
            dx = king_pos[0] - rows[idx]
            dy = king_pos[1] - cols[idx]

            pieceType = foe_piece.pieceType
            if pieceType in {'Bishop', 'Rook'}:
                if not foe_piece.is_reachable(king_pos, objgameboard):
                    continue
            elif abs(dx) <= 1 and abs(dy) <= 1:     # King, GGeneral, SGeneral, Pawn.
                if not foe_piece.is_reachable(king_pos):
                    continue
            else:
                continue
            return True

        return False

    def isInCheck(self, is_for_enemy=False):
        king_pos       = self.get_King_Pos(is_for_enemy)
        obj_the_player = self.__obj_player if not is_for_enemy else self.__obj_enemy
        objgameboard   = self.__objgameboard

        avoidCheckMoves = set()
        avoidCheckDrops = set()

        if not king_pos:
            return True, avoidCheckMoves, avoidCheckDrops

        check, set_enemy_moves, set_threatening_pos, set_block_pos = self.__is_in_check(
            king_pos, obj_the_player)

        if not check:
            return False, avoidCheckMoves, avoidCheckDrops

        # The followings will try to make the King keep away from being threatened.

        king_moves = objgameboard[king_pos].moves(objgameboard)
        # Potential moves for King to avoid checkmate.
        king_moves = set(king_moves) - set_enemy_moves
        # Check that our King will not be moved to threatened grids.
        for dest_pos in king_moves:
            piece_dest = objgameboard[dest_pos]
            objgameboard[dest_pos] = objgameboard[king_pos]
            objgameboard[dest_pos].position = dest_pos
            objgameboard[king_pos] = None
            check_after_king_move = self.__is_in_check_lite(dest_pos, obj_the_player)
            # King in new position isn't in check, so it's a legal move.
            if not check_after_king_move:
                avoidCheckMoves.add((king_pos, dest_pos))
            objgameboard[king_pos] = objgameboard[dest_pos]
            objgameboard[king_pos].position = king_pos
            objgameboard[dest_pos] = piece_dest

        # Find all of our pieces (except King) on board which could be moved to block the check.
        rows, cols = objgameboard.nonzero()
        pieces = objgameboard[(rows, cols)]
        for idx in range(len(pieces)):
            piece = pieces[idx]
            if piece.team != obj_the_player.team or piece.pieceType == 'King':
                continue
            init_pos = piece.position
            moves = piece.moves(objgameboard)
            for dest_pos in moves:
                if dest_pos not in set_block_pos and dest_pos not in set_threatening_pos:
                    continue
                # Try to move this piece to dest_pos to block the check,
                # and examine if King is still in check after that.
                piece_dest = objgameboard[dest_pos]
                objgameboard[dest_pos] = objgameboard[init_pos]
                objgameboard[dest_pos].position = dest_pos
                objgameboard[init_pos] = None
                check_after_move = self.__is_in_check_lite(king_pos, obj_the_player)
                if not check_after_move:
                    avoidCheckMoves.add((init_pos, dest_pos))
                objgameboard[init_pos] = objgameboard[dest_pos]
                objgameboard[init_pos].position = init_pos
                objgameboard[dest_pos] = piece_dest

        # Find all pieces in our capture space which could be dropped to block the check.
        promotionZone = 0 if obj_the_player.team == -1 else 4
        for str_piece in obj_the_player.captured:
            if not len(set_block_pos):
                # No any grids between the King and threatening Bishops/Rooks (if exist).
                # Dropping simulation is done. It is not needed to create any piece objects.
                break
            # Create an object once for a PIECE, not for a POSITION.
            # The position attribute will be filled in later.
            obj_piece = self.__pieceDict[str_piece](
                obj_the_player.team, None)
            for drop_pos in set_block_pos:
                if str_piece in {'p', 'P'} and drop_pos[1] == promotionZone:
                    # Illegal to drop a pawn into the promotion zone.
                    continue
                # Try to drop this piece to drop_pos to block the check,
                # and examine if King is still in check after that.
                objgameboard[drop_pos] = obj_piece
                objgameboard[drop_pos].position = drop_pos
                check_after_drop = self.__is_in_check_lite(king_pos, obj_the_player)
                if not check_after_drop:
                    avoidCheckDrops.add((str_piece, drop_pos))
                objgameboard[drop_pos] = None

        return check, avoidCheckMoves, avoidCheckDrops

    __dict_pos_to_str_for_action = {}
    @classmethod
    def __get_str_move(cls, init_pos, dest_pos, is_rotated=False):
        """It also handles rotated moves."""
        try:    return cls.__dict_pos_to_str_for_action[(init_pos, dest_pos, is_rotated)]
        except: pass

        str_init = cls.parseToString(init_pos)
        str_dest = cls.parseToString(dest_pos)
        str_move = 'move %s %s' % (str_init, str_dest)
        cls.__dict_pos_to_str_for_action[(init_pos, dest_pos, False)] = str_move
        # Precompute rotated move string.
        str_rotate_move = cls._rotateActionMove(str_move)
        cls.__dict_pos_to_str_for_action[(init_pos, dest_pos, True)] = str_rotate_move
        return str_move if not is_rotated else str_rotate_move

    @classmethod
    def __get_str_drop(cls, str_piece, dest_pos, is_rotated=False):
        """It also handles rotated moves."""
        try:    return cls.__dict_pos_to_str_for_action[(str_piece, dest_pos, is_rotated)]
        except: pass

        str_dest = cls.parseToString(dest_pos)
        str_drop = 'drop %s %s' % (str_piece, str_dest)
        cls.__dict_pos_to_str_for_action[(str_piece, dest_pos, False)] = str_drop
        # Precompute rotated drop string.
        str_rotate_drop = cls._rotateActionMove(str_drop)
        cls.__dict_pos_to_str_for_action[(str_piece, dest_pos, True)] = str_rotate_drop
        return str_drop if not is_rotated else str_rotate_drop

    def getStrMoves(self):
        # MCTS.search() -> Game.getValidMoves() -> Board.get_all_legal_moves() -> this.
        team_code    = self.__obj_player.team
        objgameboard = self.__objgameboard
        promotionZone = 0 if team_code == -1 else 4

        legalMoves = []
        rows, cols = objgameboard.nonzero()
        pieces = objgameboard[(rows, cols)]
        for idx in range(len(pieces)):
            piece = pieces[idx]
            if piece.team != team_code:
                continue
            j = cols[idx]
            init_pos = piece.position
            moves = piece.moves(objgameboard)
            for dest_pos in moves:
                str_move = self.__get_str_move(init_pos, dest_pos, self.is_rotated)
                legalMoves.append(str_move)
                if not piece.promoted and promotionZone in {j, dest_pos[1]}:
                    # init_pos or dest_pos is in promotionZone.
                    str_move += ' promote'
                    if piece.pieceType not in {'King', 'GGeneral'}:
                        legalMoves.append(str_move)
        return legalMoves

    def getStrDrops(self):
        # MCTS.search() -> Game.getValidMoves() -> Board.get_all_legal_moves() -> this.
        obj_player   = self.__obj_player
        team_code    = obj_player.team
        set_captured = set(obj_player.captured)
        objgameboard = self.__objgameboard

        if len(set_captured) == 0:
            return []

        str_pawn = 'p' if team_code == 1 else 'P'
        if str_pawn in set_captured:
            is_to_drop_pawn = True
            # Rule 2.
            promotionZone = 0 if team_code == -1 else 4
            # Rule 3.
            KING_pos = self.get_King_Pos(is_for_enemy=True)
            # Rule 1.
            pawn_pos = None
            for piece in self.__list_pawn:
                if piece.team == team_code:
                    pawn_pos = piece.position
            pawn_pos_x = -1 if not pawn_pos else pawn_pos[0]

            set_captured.remove(str_pawn)
        else:
            is_to_drop_pawn = False

        legalDrops = []
        for x in range(5):
            if is_to_drop_pawn and x == pawn_pos_x:
                if len(set_captured) == 0:
                    # Only Pawn can be dropped.
                    continue

            for y in range(5):
                if objgameboard[x, y]:
                    continue

                dest_pos = (x, y)
                for str_piece in set_captured:
                    str_drop = self.__get_str_drop(str_piece, dest_pos, self.is_rotated)
                    legalDrops.append(str_drop)

                if is_to_drop_pawn:
                    # Rule 1: Two Pawns (Nifu).
                    if x == pawn_pos_x:
                        continue
                    # Rule 2: Non-movable Piece (Iki Dokoro no nai Koma).
                    if y == promotionZone:
                        continue
                    # Rule 3: Drop Pawn Mate (Uchifu Zume).
                    if 0 <= team_code*(KING_pos[1] - y) <= 2 and abs(KING_pos[0] - x) <= 1:
                        objgameboard[x, y] = Pawn(team_code, (x, y))
                        check_after_drop, d1, d2 = self.isInCheck(is_for_enemy=True)
                        objgameboard[x, y] = None
                        # It's not allowed to drop a Pawn such that the foe King was checkmated.
                        if check_after_drop and len(d1) + len(d2) == 0:
                            continue
                    # It's valid to drop a Pawn here.
                    str_drop = self.__get_str_drop(str_pawn, dest_pos, self.is_rotated)
                    legalDrops.append(str_drop)

        return legalDrops

    def get_King_Pos(self, is_for_enemy=False):
        team_code    = (self.__obj_player if not is_for_enemy else self.__obj_enemy).team
        objgameboard = self.__objgameboard

        for piece in self.__list_king:  # Just need to examine 2 pieces!
            if piece.team == team_code:
                # Note: if the king has been taken,
                #       its position should be None @ execute_move().
                return piece.position

        return None

    ### Would be deprecated. ###
    ### Required by nothing. ###
    def get_All_Pieces_Pos(self, is_for_enemy=False):
        team_code = (self.__obj_player if not is_for_enemy else self.__obj_enemy).team
        objgameboard = self.__objgameboard

        # Find all pieces position except king.
        all_pieces_pos = []
        rows, cols = objgameboard.nonzero()
        pieces = objgameboard[(rows, cols)]
        for idx in range(len(pieces)):
            piece = pieces[idx]
            if piece.team == team_code:
                all_pieces_pos.append(piece.position)

        return all_pieces_pos

    def get_avoid_checkedOurKing_moves(self):
        team_code    = self.__obj_player.team
        objgameboard = self.__objgameboard
        promotionZone = 0 if team_code == -1 else 4

        legalMoves = []

        is_ourKing_inCheck, avoidCheckMoves, avoidCheckDrops = self.isInCheck()

        if not is_ourKing_inCheck:
            return legalMoves   # An empty list.

        if len(avoidCheckMoves) + len(avoidCheckDrops) == 0:
            return legalMoves   # An empty list.

        for move_pair in avoidCheckMoves:
            init_pos, dest_pos = move_pair
            str_move = self.__get_str_move(init_pos, dest_pos, self.is_rotated)
            legalMoves.append(str_move)
            # init_pos or dest_pos will enter promotionZone
            piece = objgameboard[init_pos]
            if not piece.promoted and promotionZone in {init_pos[1], dest_pos[1]}:
                str_move += ' promote'
                if piece.pieceType not in {'King', 'GGeneral'}:
                    legalMoves.append(str_move)

        for drop_pair in avoidCheckDrops:
            str_piece, dest_pos = drop_pair
            str_drop = self.__get_str_drop(str_piece, dest_pos, self.is_rotated)
            legalMoves.append(str_drop)

        return legalMoves

    # Check other player king and pieces.
    def get_check_otherPlayerPieces_moves(self):
        obj_player   = self.__obj_player
        team_code    = obj_player.team
        set_captured = set(obj_player.captured)
        obj_enemy    = self.__obj_enemy
        objgameboard = self.__objgameboard

        promotionZone = 0 if team_code == -1 else 4
        KING_pos = self.get_King_Pos(is_for_enemy=True)

        legalMoves = []
        legalDrops = []

        # Pruned getStrMoves().
        # These actions will
        # 1. Capturing moves: capture one of the foe pieces, or
        # 2. Giving check: make the foe King be in check.
        rows, cols = objgameboard.nonzero()
        pieces = objgameboard[(rows, cols)]
        for idx in range(len(pieces)):
            piece = pieces[idx]
            if piece.team != team_code:
                continue
            j = cols[idx]
            init_pos = piece.position
            moves = piece.moves(objgameboard)
            for dest_pos in moves:
                ### PRUNING EXAMINATION. BEG ###
                is_progressive = False
                # 1. Capturing moves.
                if not is_progressive:
                    piece_dest = objgameboard[dest_pos]
                    if piece_dest and piece_dest.team != team_code:
                        is_progressive = True
                # 2. Giving check.
                if not is_progressive:
                    piece_dest = objgameboard[dest_pos]
                    objgameboard[dest_pos] = objgameboard[init_pos]
                    objgameboard[dest_pos].position = dest_pos
                    objgameboard[init_pos] = None

                    check_after_move = self.__is_in_check_lite(KING_pos, obj_enemy)

                    objgameboard[init_pos] = objgameboard[dest_pos]
                    objgameboard[init_pos].position = init_pos
                    objgameboard[dest_pos] = piece_dest

                    if check_after_move:
                        is_progressive = True
                # Examine if meets one of conditions.
                # If not, the action should be pruned.
                if not is_progressive:
                    continue
                ### PRUNING EXAMINATION. END ###
                str_move = self.__get_str_move(init_pos, dest_pos, self.is_rotated)
                legalMoves.append(str_move)
                if not piece.promoted and promotionZone in {j, dest_pos[1]}:
                    # init_pos or dest_pos is in promotionZone.
                    str_move += ' promote'
                    if piece.pieceType not in {'King', 'GGeneral'}:
                        legalMoves.append(str_move)

        # No any pieces to drop. Early returning.
        if len(set_captured) == 0:
            return legalMoves

        # Pruned getStrDrops().
        # These actions will make some foe pieces be threatened.
        str_pawn = 'p' if team_code == 1 else 'P'
        if str_pawn in set_captured:
            is_to_drop_pawn = True
            # Rule 2.
            promotionZone = 0 if team_code == -1 else 4
            # Rule 3.
            KING_pos = self.get_King_Pos(is_for_enemy=True)
            # Rule 1.
            pawn_pos = None
            for piece in self.__list_pawn:
                if piece.team == team_code:
                    pawn_pos = piece.position
            pawn_pos_x = -1 if not pawn_pos else pawn_pos[0]

            set_captured.remove(str_pawn)
        else:
            is_to_drop_pawn = False

        for x in range(5):
            if is_to_drop_pawn and x == pawn_pos_x:
                if len(set_captured) == 0:
                    # Only Pawn can be dropped.
                    continue

            for y in range(5):
                if objgameboard[x, y]:
                    continue

                dest_pos = (x, y)
                for str_piece in set_captured:
                    ### PRUNING EXAMINATION. BEG ###
                    # To make some foe pieces be threatened.
                    objgameboard[x, y] = self.__pieceDict[str_piece](
                        team_code, dest_pos)
                    moves = objgameboard[x, y].moves(objgameboard)
                    objgameboard[x, y] = None
                    is_threatening = False
                    for threaten_pos in moves:
                        piece_threaten = objgameboard[threaten_pos]
                        if not piece_threaten or piece_threaten.team == team_code:
                            continue
                        is_threatening = True
                        break
                    if not is_threatening:
                        continue
                    ### PRUNING EXAMINATION. END ###
                    str_drop = self.__get_str_drop(str_piece, dest_pos, self.is_rotated)
                    legalDrops.append(str_drop)

                if is_to_drop_pawn:
                    # Rule 1: Two Pawns (Nifu).
                    if x == pawn_pos_x:
                        continue
                    # Rule 2: Non-movable Piece (Iki Dokoro no nai Koma).
                    if y == promotionZone:
                        continue
                    # Rule 3: Drop Pawn Mate (Uchifu Zume).
                    if 0 <= team_code*(KING_pos[1] - y) <= 2 and abs(KING_pos[0] - x) <= 1:
                        objgameboard[x, y] = Pawn(team_code, (x, y))
                        check_after_drop, d1, d2 = self.isInCheck(is_for_enemy=True)
                        objgameboard[x, y] = None
                        # It's not allowed to drop a Pawn such that the foe King was checkmated.
                        if check_after_drop and len(d1) + len(d2) == 0:
                            continue
                    ### PRUNING EXAMINATION. BEG ###
                    # To make some foe pieces be threatened.
                    threaten_pos = (x, y+1) if team_code == 1 else (x, y-1)
                    piece_threaten = objgameboard[threaten_pos]
                    if not piece_threaten or piece_threaten.team == team_code:
                        continue
                    ### PRUNING EXAMINATION. END ###
                    # It's valid to drop a Pawn here.
                    str_drop = self.__get_str_drop(str_pawn, dest_pos, self.is_rotated)
                    legalDrops.append(str_drop)

        legalMoves.extend(legalDrops)

        return legalMoves

    def get_all_legal_moves(self):
        legalMoves = self.getStrMoves()
        legalMoves.extend(self.getStrDrops())
        return legalMoves

    def execute_move(self, str_move: str):
        obj_player   = self.__obj_player
        obj_enemy    = self.__obj_enemy
        objgameboard = self.__objgameboard
        promotionZone = 0 if obj_player.team == -1 else 4

        if self.is_rotated:
            str_move = self._rotateActionMove(str_move)
        str_act_type, init_pos, dest_pos, promote = self._move_parser(str_move)
        if str_act_type == 'move':
            piece = objgameboard[init_pos]
            # viable piece
            if not (piece and piece.inBounds(init_pos) and piece.inBounds(dest_pos)):
                raise Exception('Impossible move found in execute_move().')
            moves = piece.moves(objgameboard)
            if dest_pos not in moves:
                from minishogi.MiniShogiGame import display
                print_func = logging.debug
                print_func('Illegal move. 1')
                print_func('move: ' + str(str_move))
                print_func('piece: ' + str(piece.pieceType))
                print_func('piece.team: ' + str(piece.team))
                print_func('dest_pos: ' + str(dest_pos))
                print_func('moves: ' + str(moves))
                display(objgameboard, print_func=print_func,
                    str_prefix='Board:\n')
                raise loggingException('dest_pos are logged.')
            # Promotion rules
            # Pawn automatically promotes if it is in the promotion zone.
            if piece.pieceType == 'Pawn' and dest_pos[1] == promotionZone:
                promote = not piece.promoted
                piece.promote()
            elif promote:
                if promotionZone in {init_pos[1], dest_pos[1]} and not piece.promoted:
                    piece.promote()
                else:
                    raise Exception('Invalid promotion.')
            # Make a move to a blank space or capture.
            piece_dest = objgameboard[dest_pos]
            if piece_dest:
                # Move to a space that has a piece on it, capture!
                str_piece = piece_dest.pieceType[0]     # Upper case.
                # To prevent piece cache from accessing this captured piece.
                if str_piece == 'P':
                    self.__list_pawn.remove(piece_dest)
                else:
                    piece_dest.position = None
                # Record str_piece for the turn player.
                if obj_player.team == 1:
                    str_piece = str_piece.lower()
                obj_player.capture(str_piece)
            # The piece at init_pos is going to be moved to dest_pos.
            objgameboard[dest_pos] = piece
            objgameboard[dest_pos].position = dest_pos      # set new position
            objgameboard[init_pos] = None
            self.__cache_move_updater(piece, piece_dest, init_pos, dest_pos, promote)
        elif str_act_type == 'drop':
            str_piece = init_pos
            # Drop piece onto square.
            obj_piece = self.__pieceDict[str_piece](obj_player.team, dest_pos)
            if str_piece in {'p', 'P'}:
                self.__list_pawn.append(obj_piece)
            objgameboard[dest_pos] = obj_piece
            # logging.debug('Drop: ' + str_piece)
            obj_player.drop(str_piece)
            self.__cache_drop_updater(str_piece, dest_pos)
        else:
            print(str(obj_enemy.team) + ' player wins. Illegal move. 2')

    @classmethod
    def getNNGameBoard(cls, str_or_obj_gameboard: np.ndarray, obj_player1: Player, obj_player2: Player):
        """

        Compatible with strgameboard and objgameboard!
        Note: Regardless the order of players in parameters, it will yield the same result.

        Arguments:
            str_or_obj_gameboard {np.ndarray} -- a numpy array which has str/Pieces entries.
            obj_player1 {Player} -- A player.
            obj_player2 {Player} -- Another player.

        Returns:
            np.ndarray -- a numpy array which has 1/-1/0 entries.
        """
        nngameboard = np.zeros((5, 5, 40), dtype=np.int8)

        if str_or_obj_gameboard.dtype not in {np.dtype('U2'), np.dtype(object)}:
            raise 'getNNGameBoard(): elements of the board should be as U2 or object!'

        is_str = (str_or_obj_gameboard.dtype == np.dtype('U2'))
        strgameboard = str_or_obj_gameboard

        rows, cols = strgameboard.nonzero()
        pieces = strgameboard[(rows, cols)]
        # Normal board
        for idx in range(len(pieces)):
            i = rows[idx]
            j = cols[idx]
            if is_str:
                str_piece = pieces[idx]
            else:
                # For OBJ game board.
                str_piece = pieces[idx].pieceType[0]    # The 1st char of class name (upper).
                if pieces[idx].team == 1:
                    str_piece = str_piece.lower()
                if pieces[idx].promoted:
                    str_piece = '+' + str_piece
            k = cls.pieceDict[str_piece]

            nngameboard[i, j, k] = 1 if str_piece[-1].islower() else -1     # str_piece: /+?\w/

        # Capture board
        for obj_player in (obj_player1, obj_player2):
            player = obj_player.team
            list_captured = obj_player.captured
            for piece in list_captured:
                if piece.lower() == 'k':
                    continue

                k = cls.pieceDict['c' + piece]
                if nngameboard[0, 0, k]:
                    # c_ -> c2_. Two pieces of same type are captured.
                    nngameboard[:, :, k+10] = player
                else:
                    nngameboard[:, :, k] = player

        return nngameboard

    @classmethod
    def getObjGameBoard(cls, nngameboard: np.ndarray):
        return cls.__getOGB(nngameboard)[0]

    @classmethod
    def __getOGB(cls, nngameboard: np.ndarray):
        objgameboard = np.empty(shape=(5, 5), dtype=object)
        list_king = []
        list_pawn = []

        rows, cols, pieces = nngameboard[:, :, :20].nonzero()   # Captured pieces are excluded.
        vals = nngameboard[(rows, cols, pieces)]
        # Normal pieces
        for idx in range(len(pieces)):
            i = rows[idx]           # Row.
            j = cols[idx]           # Column.
            k = pieces[idx] % 10    # Piece.
            p = vals[idx]           # Player.

            cls_piece = cls.__pieceOrder[k]
            is_promoted = (k >= 6)
            objgameboard[i, j] = cls_piece(p, (i, j), is_promoted)

            if k == 0:
                # Record the King object for convenience.
                list_king.append(objgameboard[i, j])
            elif k == 5 or k == 9:
                # Record the unpromoted/promoted Pawn object for convenience.
                list_pawn.append(objgameboard[i, j])

        # print(objgameboard)
        return objgameboard, list_king, list_pawn

    @classmethod
    def getStrGameBoard(cls, objgameboard: np.ndarray):
        strgameboard = np.empty(shape=(5, 5), dtype='U2')   # Unicode string with length 2.

        rows, cols = objgameboard.nonzero()
        pieces = objgameboard[(rows, cols)]
        # Normal pieces
        for idx in range(len(pieces)):
            i = rows[idx]
            j = cols[idx]
            str_piece = pieces[idx].pieceType[0]    # The 1st char of class name (upper).

            if pieces[idx].team == 1:
                str_piece = str_piece.lower()

            if pieces[idx].promoted:
                str_piece = '+' + str_piece

            strgameboard[i, j] = str_piece

        return strgameboard

    @classmethod
    def getObjGameCapture(cls, nngameboard: np.ndarray, player: int) -> list:
        captured_1, captured_2 = cls.__getOGC(nngameboard, player)
        return captured_1

    @classmethod
    def __getOGC(cls, nngameboard: np.ndarray, player: int):
        captured_1, captured_2 = [], []
        nngameboard_captured = nngameboard[0, 0, 20: ]
        pieces, = nngameboard_captured.nonzero()    # Captured pieces are selected. tuple[0] -> 1d array.
        vals = nngameboard_captured[pieces]
        for idx in range(len(pieces)):
            k = pieces[idx] % 5
            p = vals[idx]

            str_piece = cls.__strOrder[k]
            captured = captured_1 if (p == player) else captured_2
            captured.append(str_piece if p == 1 else str_piece.upper())

        return captured_1, captured_2
