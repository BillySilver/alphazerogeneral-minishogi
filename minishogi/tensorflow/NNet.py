from .MiniShogiNNet import MiniShogiNNet as onnet
import tensorflow as tf
from NeuralNet import NeuralNet
from pytorch_classification.utils import Bar, AverageMeter
from utils import *
import os
import shutil
import time
import random
import numpy as np
import math
import sys
sys.path.append('../../')


args = dotdict({
    'lr': 0.001,
    'l2_coef': 1e-4,
    'epochs': 10,
    'batch_size': 64,
    'num_channels': 32,
    'num_blocks': 5,
})


class NNetWrapper(NeuralNet):
    def __init__(self, game):
        self.nnet = onnet(game, args)
        self.board_x, self.board_y, self.board_z = game.getBoardSize()
        self.action_size = game.getActionSize()

        # Set gpu memory grow
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.Session(config=config, graph=self.nnet.graph)

        self.saver = None
        with tf.Session() as temp_sess:
            temp_sess.run(tf.global_variables_initializer())
        self.sess.run(tf.variables_initializer(self.nnet.graph.get_collection('variables')))


    def train(self, examples):
        """
        examples: list of examples, each example is of form (board, pi, v)
        """

        for epoch in range(args.epochs):
            print('EPOCH ::: ' + str(epoch+1))
            data_time = AverageMeter()
            batch_time = AverageMeter()
            pi_losses = AverageMeter()
            v_losses = AverageMeter()
            l2_losses = AverageMeter()
            end = time.time()

            is_compressing = (2 == len(examples[0][0].shape))  # list[0] -> tuple[0] -> (n_board_h, n_board_w, n_piece)/(n_sample, n_board_h*n_board_w*n_piece).
            if False == is_compressing:
                # For original data.
                example_ids = list(range(len(examples)))
            else:
                # For compressed data.
                _boards, _pis, _vs = list(zip(*examples))   # Grouped by iteration.
                example_ids = list(range(sum([ len(vs_iter) for vs_iter in _vs ])))
                _boards = np.vstack([
                    # 1) (?, 525) -> (?, 5, 5, 21) -> P:(?, 5, 5, 20) / C:(?, 5, 5, 1) in lambda.
                    # 2) C:(?, 5, 5, 1) -> C:(?, 1, 1, 20). The last 5 elements are dummy.
                    # 3) tile C:(?, 1, 1, 20) -> C:(?, 5, 5, 20).
                    # 4) concatenate P and C.
                    np.concatenate(
                        (lambda x: ( x[:, :, :, :20],
                                     np.tile(x[:, :, :, 20].reshape(-1, 1, 1, 25)[:, :, :, :20], reps=(1, 5, 5, 1)) )
                        )( boards_iter.toarray().reshape(-1, 5, 5, 21) ) ,
                        axis=-1)
                    for boards_iter in _boards ])
                _pis    = np.vstack([ pis_iter for pis_iter in _pis ])
                _vs     = np.hstack([ vs_iter for vs_iter in _vs ])
            np.random.shuffle(example_ids)

            bar = Bar('Training Net', max=-(-len(example_ids)//args.batch_size))    # Round up.
            batch_idx = 0

            # self.sess.run(tf.local_variables_initializer())
            while batch_idx*args.batch_size < len(example_ids):
                sample_ids = example_ids[batch_idx*args.batch_size :
                                         (batch_idx+1)*args.batch_size]
                if False == is_compressing:
                    # For original data.
                    boards, pis, vs = list(zip(*[examples[i] for i in sample_ids]))
                else:
                    # For compressed data.
                    boards, pis, vs = _boards[sample_ids], _pis[sample_ids], _vs[sample_ids]

                # predict and compute gradient and do SGD step
                input_dict = {self.nnet.input_boards: boards, self.nnet.target_pis: pis,
                              self.nnet.target_vs: vs, self.nnet.isTraining: True}

                # measure data loading time
                data_time.update(time.time() - end)

                # record loss
                self.sess.run(self.nnet.train_step, feed_dict=input_dict)
                pi_loss, v_loss, l2_loss = self.sess.run(
                    [self.nnet.loss_pi, self.nnet.loss_v, self.nnet.loss_l2], feed_dict=input_dict)
                pi_losses.update(pi_loss, len(boards))
                v_losses.update(v_loss, len(boards))
                l2_losses.update(l2_loss, len(boards))

                # measure elapsed time
                batch_time.update(time.time() - end)
                end = time.time()
                batch_idx += 1

                # plot progress
                bar.suffix = '({batch}/{size}) Data: {data:.3f}s | Batch: {bt:.3f}s | Total: {total:} | ETA: {eta:} | Loss_pi: {lpi:.4f} | Loss_v: {lv:.4f} | Loss_l2: {ll2:.4f}'.format(
                    batch=min(batch_idx*args.batch_size, len(example_ids)),
                    size=len(example_ids),
                    data=data_time.avg,
                    bt=batch_time.avg,
                    total=bar.elapsed_td,
                    eta=bar.eta_td,
                    lpi=pi_losses.avg,
                    lv=v_losses.avg,
                    ll2=l2_losses.avg
                )
                bar.next()
            bar.finish()

    def predict(self, board):
        """
        board: np array with board
        """
        # timing
        start = time.time()

        # preparing input
        board = board[np.newaxis, :, :, :]

        # run
        prob, v = self.sess.run([self.nnet.prob, self.nnet.v], feed_dict={
                                self.nnet.input_boards: board, self.nnet.dropout: 0, self.nnet.isTraining: False})

        #print('PREDICTION TIME TAKEN : {0:03f}'.format(time.time()-start))
        return prob[0], v[0]

    def save_checkpoint(self, folder='checkpoint', filename='checkpoint.pth.tar'):
        filepath = os.path.join(folder, filename)
        if not os.path.exists(folder):
            print(
                "Checkpoint Directory does not exist! Making directory {}".format(folder))
            os.mkdir(folder)
        else:
            print("Checkpoint Directory exists! ")
        if self.saver == None:
            self.saver = tf.train.Saver(
                self.nnet.graph.get_collection('variables'))
        with self.nnet.graph.as_default():
            self.saver.save(self.sess, filepath)

    def load_checkpoint(self, folder='checkpoint', filename='checkpoint.pth.tar'):
        filepath = os.path.join(folder, filename)
        if not os.path.exists(filepath+'.meta'):
            raise("No model in path {}".format(filepath))
        with self.nnet.graph.as_default():
            self.saver = tf.train.Saver()
            self.saver.restore(self.sess, filepath)
