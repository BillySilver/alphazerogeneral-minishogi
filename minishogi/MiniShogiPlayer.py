class Player:
    def __init__(self, team, captured):
        self.team = team
        self.captured = captured

    # pieces that the player has captured, printed every turn
    def capture(self, piece):
        self.captured.append(piece)

    # piece that has been placed on to the board and removed from captured
    def drop(self, piece):
        self.captured.remove(piece)
