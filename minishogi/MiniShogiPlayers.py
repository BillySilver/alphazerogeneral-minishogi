import numpy as np
from minishogi.MiniShogiLogic import Board
from minishogi.MiniShogiGame import all_valid_move


class HumanMiniShogiPlayer():
    def __init__(self, game):
        self.game = game
        self.board = Board  # No instance created. Using class methods below.

    def play(self, board, round=1):
        # print("display:")
        # display(board)
        print("Round:", round)
        valids = self.game.getValidMoves(board, 1, 0, round)
        b = self.board(board, 1)
        strgameboard = b.strgameboard

        str_moves = []
        valid_acts, = valids.nonzero()
        for a in valid_acts:
            str_move = all_valid_move[a]
            str_move_org_split = str_move.split()
            if round % 2 == 0:
                str_move = self.board._rotateActionMove(str_move)
            str_moves.append(str_move)
            print('[%2d] %s' % (len(str_moves), str_move), end='')
            if str_move_org_split[0] == 'move':
                init_pos = self.board.parsePos(str_move_org_split[1])
                move_pos = self.board.parsePos(str_move_org_split[2])
                str_piece_1 = strgameboard[init_pos]
                str_piece_2 = strgameboard[move_pos]
                if round % 2 == 0:
                    str_piece_1 = str_piece_1.swapcase()
                    str_piece_2 = str_piece_2.swapcase()
                print('  (%s -> %s)' % (str_piece_1, str_piece_2))
            else:
                print('')

        while True:
            try:
                str_input = input('Choose an action: ').strip()
                if str_input in str_moves:
                    str_move = str_input
                else:
                    n_input = int(str_input)
                    if not (1 <= n_input <= len(str_moves)):
                        print("Invalid action, so input again.")
                        continue
                    str_move = str_moves[n_input - 1]
            except ValueError:
                print("Invalid action, so input again.")
                continue

            print('* Action: %s' % str_move)
            if round % 2 == 0:
                str_move = self.board._rotateActionMove(str_move)

            try:
                action = self.game.moveDict[str_move]
            except:
                print("Invalid action, so input again.")
                continue

            if valids[action]:
                break
            else:
                print("Illegal move:", str_move)

        return action
