from .MiniShogiGame import *
from .MiniShogiLogicSlow import *

class MiniShogiGameSlow(MiniShogiGame):
    def __init__(self):
        self.pieceDict = {'k': 0, 'g': 1, 's': 2, 'b': 3, 'r': 4, 'p': 5, '+s': 6, '+b': 7, '+r': 8, '+p': 9,
                     'K': 10, 'G': 11, 'S': 12, 'B': 13, 'R': 14, 'P': 15, '+S': 16, '+B': 17, '+R': 18, '+P': 19,
                     'cg': 20, 'cs': 21, 'cb': 22, 'cr': 23, 'cp': 24, 'cG': 25, 'cS': 26, 'cB': 27, 'cR': 28, 'cP': 29,
                     'c2g': 30, 'c2s': 31, 'c2b': 32, 'c2r': 33, 'c2p': 34, 'c2G': 35, 'c2S': 36, 'c2B': 37, 'c2R': 38, 'c2P': 39}

    def getNNGameBoard(self, strgameboard, objplayer, otherobjplayer):
        nngameboard = np.zeros((5, 5, 40), dtype=np.int16)

        # Normal board
        for i in range(0, 5):
            for j in range(0, 5):
                if strgameboard[i][j] != '':
                    if len(strgameboard[i][j]) == 2:
                        strgameboardsplit = strgameboard[i][j].split('+')
                        if strgameboardsplit[1].islower():
                            nngameboard[i][j][self.pieceDict[strgameboard[i][j]]] = 1
                        else:
                            nngameboard[i][j][self.pieceDict[strgameboard[i][j]]] = -1
                    else:
                        if strgameboard[i][j].islower():
                            nngameboard[i][j][self.pieceDict[strgameboard[i][j]]] = 1
                        else:
                            nngameboard[i][j][self.pieceDict[strgameboard[i][j]]] = -1

        ourCaptured = objplayer.captured
        otherCaptured = otherobjplayer.captured

        # Capture board
        if ourCaptured:
            for piece in ourCaptured:
                if piece.lower() == 'k':
                    continue
                for i in range(0, 5):
                    for j in range(0, 5):
                        if nngameboard[i][j][self.pieceDict['c' + piece]]:
                            nngameboard[i][j][self.pieceDict['c2' + piece]] = 1
                        else:
                            nngameboard[i][j][self.pieceDict['c' + piece]] = 1

        if otherCaptured:
            for piece in otherCaptured:
                if piece.lower() == 'k':
                    continue
                for i in range(0, 5):
                    for j in range(0, 5):
                        if nngameboard[i][j][self.pieceDict['c' + piece]]:
                            nngameboard[i][j][self.pieceDict['c2' + piece]] = -1
                        else:
                            nngameboard[i][j][self.pieceDict['c' + piece]] = -1

        return nngameboard

    def getObjGameBoard(self, nngameboard):
        objgameboard = [[None for j in range(5)] for i in range(5)]
        pieceOrder = [King, GGeneral, SGeneral, Bishop, Rook, Pawn]

        # Normal pieces
        for i in range(0, 5):
            for j in range(0, 5):
                for k in range(0, 20):
                    if nngameboard[i][j][k] == 1:
                        if k >= 0 and k < 6:
                            objgameboard[i][j] = pieceOrder[k](1, str(pieceOrder[k].__name__), (i, j), 1, False)
                            continue
                        if k >= 6 and k < 10:
                            objgameboard[i][j] = pieceOrder[k-4](1, str(pieceOrder[k-4].__name__), (i, j), 1, True)
                            continue
                        if k >= 10 and k < 16:
                            objgameboard[i][j] = pieceOrder[k-10](1, str(pieceOrder[k-10].__name__), (i, j), 1, False)
                            continue
                        if k >= 16 and k < 20:
                            objgameboard[i][j] = pieceOrder[k-14](1, str(pieceOrder[k-14].__name__), (i, j), 1, True)
                            continue
                    elif nngameboard[i][j][k] == -1:
                        if k >= 0 and k < 6:
                            objgameboard[i][j] = pieceOrder[k](-1, str(pieceOrder[k].__name__), (i, j), -1, False)
                            continue
                        if k >= 6 and k < 10:
                            objgameboard[i][j] = pieceOrder[k-4](-1, str(pieceOrder[k-4].__name__), (i, j), -1, True)
                            continue
                        if k >= 10 and k < 16:
                            objgameboard[i][j] = pieceOrder[k-10](-1, str(pieceOrder[k-10].__name__), (i, j), -1, False)
                            continue
                        if k >= 16 and k < 20:
                            objgameboard[i][j] = pieceOrder[k-14](-1, str(pieceOrder[k-14].__name__), (i, j), -1, True)
                            continue
                    else:
                        continue

        # print(objgameboard)
        return objgameboard

    def getObjGameBoard_proto(self, nngameboard):
        """
        A faster version employed.
        See current version of MiniShogiGame.getObjGameBoard().
        """
        from scipy import sparse

        objgameboard = np.empty(shape=(5, 5), dtype=object)
        pieceOrder = [King, GGeneral, SGeneral, Bishop, Rook, Pawn, SGeneral, Bishop, Rook, Pawn]

        nngameboard = sparse.csr_matrix(nngameboard.reshape(25, -1))
        rows, cols = nngameboard.nonzero()
        vals = nngameboard.data
        # Normal pieces
        for idx in range(len(vals)):
            if cols[idx] >= 20: # Captured.
                continue

            i = rows[idx] // 5  # Row.
            j = rows[idx] % 5   # Column.
            k = cols[idx] % 10  # Piece.
            p = vals[idx]       # Player.

            objgameboard[i, j] = pieceOrder[k](p, str(pieceOrder[k].__name__), (i, j), p, k >= 6)

        # print(objgameboard)
        return objgameboard

    def getStrGameBoard(self, objgameboard):
        strgameboard = [["" for j in range(5)] for i in range(5)]

        # Normal pieces
        for i in range(0, 5):
            for j in range(0, 5):
                if objgameboard[i][j] != None:
                    if objgameboard[i][j].pieceType == "King":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+k"
                            else:
                                strgameboard[i][j] = "k"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+K"
                            else:
                                strgameboard[i][j] = "K"

                    if objgameboard[i][j].pieceType == "GGeneral":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+g"
                            else:
                                strgameboard[i][j] = "g"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+G"
                            else:
                                strgameboard[i][j] = "G"

                    if objgameboard[i][j].pieceType == "SGeneral":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+s"
                            else:
                                strgameboard[i][j] = "s"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+S"
                            else:
                                strgameboard[i][j] = "S"

                    if objgameboard[i][j].pieceType == "Bishop":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+b"
                            else:
                                strgameboard[i][j] = "b"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+B"
                            else:
                                strgameboard[i][j] = "B"

                    if objgameboard[i][j].pieceType == "Rook":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+r"
                            else:
                                strgameboard[i][j] = "r"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+R"
                            else:
                                strgameboard[i][j] = "R"

                    if objgameboard[i][j].pieceType == "Pawn":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+p"
                            else:
                                strgameboard[i][j] = "p"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+P"
                            else:
                                strgameboard[i][j] = "P"

        return strgameboard

    def getObjGameCapture(self, nngameboard, player):
        strOrder = ["g", "s", "b", "r", "p"]
        captured = []
        for k in range(20, 40):
            if nngameboard[0][0][k] == player:
                if nngameboard[0][0][k] == 1:
                    captured.append(strOrder[k % 5])
                if nngameboard[0][0][k] == -1:
                    captured.append(strOrder[k % 5].upper())

        return captured

    def getCanonicalForm(self, board, player):
        """
        Input:
            board: current board
            player: current player (1 or -1)

        Returns:
            canonicalBoard: returns canonical form of board. The canonical form
                            should be independent of player. For e.g. in chess,
                            the canonical form can be chosen to be from the pov
                            of white. When the player is white, we can return
                            board as is. When the player is black, we can invert
                            the colors and return the board.
        """
        copyBoard = np.copy(board)
        if player == -1:
            # Associate -1 player with canonicalForm
            # Rotate 2 times(180 degree)
            rotateCopyBoard = np.rot90(copyBoard, 2)
            rotateCopyBoard[:][:][20:40] *= player
            # Rotate object pieces
            objgameboard = self.getObjGameBoard(rotateCopyBoard)
            # Change prison: objplayer == player -1, otherobjplayer == player 1
            objplayer = Player(player, self.getObjGameCapture(rotateCopyBoard, player))
            otherobjplayer = Player(-player, self.getObjGameCapture(rotateCopyBoard, -player))
            # Change some pieces class attribution value: team & direction 1 -> -1 and reposition
            for i in range(0, 5):
                for j in range(0, 5):
                    if objgameboard[i][j] != None:
                        objgameboard[i][j].team *= player
                        # objgameboard[i][j].direction *= player    # direction removed from class Piece.
                        objgameboard[i][j].position = (i, j)

            strgameboard = self.getStrGameBoard(objgameboard)
            rotateNNGameBoard = self.getNNGameBoard(strgameboard, objplayer, otherobjplayer)
            return rotateNNGameBoard

        objgameboard = self.getObjGameBoard(copyBoard)
        objplayer = Player(player, self.getObjGameCapture(copyBoard, player))
        otherobjplayer = Player(-player, self.getObjGameCapture(copyBoard, -player))
        strgameboard = self.getStrGameBoard(objgameboard)
        NNGameBoard = self.getNNGameBoard(strgameboard, objplayer, otherobjplayer)
        return NNGameBoard
