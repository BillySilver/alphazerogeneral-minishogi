from minishogi.tensorflow.NNet import NNetWrapper as nn
from collections import deque
from Arena import Arena
from MCTS import MCTS
import numpy as np
from pytorch_classification.utils import Bar, AverageMeter
import time
import os
import sys
import logging
from pickle import Pickler, Unpickler
from random import shuffle

import multiprocessing
multiprocessing.set_start_method('spawn', True)


def async_play(game, args, iter_num, iterr, bar):
    import datetime     # Custom verbose for profiling.

    n_finished_job = max(0, iter_num+1 - min(args.numEps, args.numSelfPlayPool))
    bar.next(n_finished_job)
    bar.suffix = "play:{i}/{x} | Total: {total:} | ETA: {eta:}".format(
        i=iter_num+1, x=iterr, total=bar.elapsed_td, eta=bar.eta_td)
    bar.next(iter_num+1 - n_finished_job)
    if iter_num + 1 == iterr:
        bar.finish()

    net = nn(game)
    mcts = MCTS(game, net, args)

    trainExamples = []
    board = game.getInitBoard()
    curPlayer = 1
    episodeStep = 0

    while True:
        episodeStep += 1
        canonicalBoard = game.getCanonicalForm(board, curPlayer)
        temp = int(episodeStep < args.tempThreshold)

        pi = mcts.getActionProb(canonicalBoard, temp=temp, round=episodeStep)
        trainExamples.append([canonicalBoard, curPlayer, pi, None])

        action = np.random.choice(len(pi), p=pi)
        board, curPlayer = game.getNextState(board, curPlayer, action)

        r = game.getGameEnded(board, curPlayer)

        if episodeStep > 200:   # Absolutely repeat repetitions.
            print("\nThe game moves = 200, so retraining.")
            trainExamples = []
            board = game.getInitBoard()
            curPlayer = 1
            episodeStep = 0
            continue

        if r != 0:
            if iter_num >= args.numEps - (args.numSelfPlayPool // 3):
                not args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', 'Job', iter_num, 'is done, will be returned.', len(trainExamples), 'steps.')
            return [(x[0], x[2], r * ((-1) ** (x[1] != curPlayer))) for x in trainExamples]


class Coach():
    """
    This class executes the self-play + learning. It uses the functions defined
    in Game and NeuralNet. args are specified in main.py.
    """

    def __init__(self, game, nnet, args):
        self.game = game
        self.nnet = nnet
        self.pnet = self.nnet.__class__(self.game)  # the competitor network
        self.args = args
        self.mcts = MCTS(self.game, self.nnet, self.args)
        # history of examples from args.numItersForTrainExamplesHistory latest iterations
        self.trainExamplesHistory = []
        self.skipFirstSelfPlay = False  # can be overriden in loadTrainExamples()

    def parallel_self_play(self):
        import datetime     # Custom verbose for profiling.

        pool = multiprocessing.Pool(processes=self.args.numSelfPlayPool)
        res = []
        result = []
        bar = Bar('Self Play', max=self.args.numEps)
        for i in range(self.args.numEps):
            import time
            time.sleep(0.1)     # Make the progress bar display better.
            res.append(pool.apply_async(async_play, args=(
                self.game, self.args, i, self.args.numEps, bar,)))
        pool.close()
        pool.join()
        not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', 'All the jobs are done. Merging results...')
        for i in res:
            result += i.get()
        if True == self.args.is_compressing:
            from scipy import sparse
            result = [( sparse.csr_matrix(np.vstack([
                            np.concatenate([
                                x[0][:, :, :20],                                        # Normal pieces on board.
                                np.append(x[0][0, 0, 20: ], [0]*5).reshape(5, 5, 1)     # Captured pieces: 0/1/-1. Stored in first 20 of (5, 5, 1).
                            ], axis=-1).reshape(1, -1)
                            for x in result ])) ,
                        np.vstack([ x[1] for x in result ]) ,
                        np.array([ x[2] for x in result ], dtype=np.int8) )]
        not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', 'Merged.')
        return result

    def learn(self):
        """
        Performs numIters iterations with numEps episodes of self-play in each
        iteration. After every iteration, it retrains neural network with
        examples in trainExamples (which has a maximium length of maxlenofQueue).
        It then pits the new neural network against the old one and accepts it
        only if it wins >= updateThreshold fraction of games.
        """
        import datetime     # Custom verbose for profiling.

        # create log file
        if not os.path.exists(self.args.checkpoint):
            os.mkdir(self.args.checkpoint)
        logging.basicConfig(filename=os.path.join(self.args.checkpoint, self.args.logName),
                            filemode='a+', level=logging.DEBUG, format='%(asctime)s  - %(levelname)-s: %(message)s')

        for i in range(len(self.trainExamplesHistory) + 1, self.args.numIters + 1):
            # bookkeeping
            print('------ITER ' + str(i) + '/' + str(self.args.numIters) + '------')
            # examples of the iteration
            iterationTrainExamples = deque([], maxlen=self.args.maxlenOfQueue)
            # reset search tree
            # self.mcts = MCTS(self.game, self.nnet, self.args)
            logging.debug("Start: Self Play")
            temp = self.parallel_self_play()
            logging.debug("End:   Self Play")
            not self.args.is_custom_verbose or print('Total steps of all jobs:', len(temp[0][1] if self.args.is_compressing else temp))     # list[0] -> tuple[1] -> np.ndarray (if compressed).
            iterationTrainExamples += temp
            # save the iteration examples to the history
            self.trainExamplesHistory.append(iterationTrainExamples)
            not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', 'trainExamplesHistory extended.')

            if len(self.trainExamplesHistory) > self.args.numItersForTrainExamplesHistory:
                print("len(trainExamplesHistory) =", len(
                    self.trainExamplesHistory), " => remove the oldest trainExamples")
                self.trainExamplesHistory.pop(0)
            # backup history to a file
            # NB! the examples were collected using the model from the previous iteration, so (i-1)
            # NB! OneNet is use to now iteration train examples, so (i)
            self.saveTrainExamples(i)
            # ^^^^^^^^^^^^^^^^^^^^^^^^^ It may cost lots of time and memory! Thus excluded temporarily.
            #                           However, if a early trained model is loaded,
            #                           there will be no history examples for training.

            # shuffle examlpes before training
            not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', 'Before obtaining trainExamples.')
            trainExamples = []
            for e in self.trainExamplesHistory:
                trainExamples.extend(e)
            shuffle(trainExamples)

            if not self.args.oneNet:
                # training new network, keeping a copy of the old one
                self.nnet.save_checkpoint(
                    folder=self.args.checkpoint, filename='temp.pth.tar')
                self.pnet.load_checkpoint(
                    folder=self.args.checkpoint, filename='temp.pth.tar')
                pmcts = MCTS(self.game, self.pnet, self.args)

                self.nnet.train(trainExamples)
                nmcts = MCTS(self.game, self.nnet, self.args)

                print('PITTING AGAINST PREVIOUS VERSION')
                arena = Arena(lambda x, round: np.argmax(pmcts.getActionProb(
                    x, temp=0, round=round)), lambda x, round: np.argmax(nmcts.getActionProb(x, temp=0, round=round)), self.game)
                pfwins, nfwins, pswins, nswins, draws = arena.playGames(
                    self.args.arenaCompare)
                pwins = pfwins + pswins
                nwins = nfwins + nswins

                print('NEW/PREV WINS : %d / %d ; DRAWS : %d' %
                      (nwins, pwins, draws))
                if pwins+nwins > 0 and float(nwins)/(pwins+nwins) < self.args.updateThreshold:
                    print('REJECTING NEW MODEL')
                    self.nnet.load_checkpoint(
                        folder=self.args.checkpoint, filename='temp.pth.tar')
                else:
                    print('ACCEPTING NEW MODEL')
                    self.nnet.save_checkpoint(
                        folder=self.args.checkpoint, filename=self.getCheckpointFile(i))
                    self.nnet.save_checkpoint(
                        folder=self.args.checkpoint, filename='best.pth.tar')
            else:
                not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', 'Before train().')
                self.nnet.train(trainExamples)
                not self.args.is_custom_verbose or print(' *', str(datetime.datetime.now().time())[ :11], ':', '', end='')
                print('COMPLETING UPDATE')
                self.nnet.save_checkpoint(
                    folder=self.args.checkpoint, filename='best_' + str(i) + '.pth.tar')

            # self.trainExamplesHistory.clear()

    def getCheckpointFile(self, iteration):
        return 'best_' + str(iteration) + '.pth.tar'

    def saveTrainExamples(self, iteration):
        folder = self.args.checkpoint
        if not os.path.exists(folder):
            os.makedirs(folder)
        filename = os.path.join(
            folder, self.getCheckpointFile(iteration) + ".examples")
        with open(filename, "wb+") as f:
            Pickler(f).dump(self.trainExamplesHistory)
        f.closed

    def loadTrainExamples(self):
        modelFile = os.path.join(
            self.args.load_folder_file[0], self.args.load_folder_file[1])
        examplesFile = modelFile + ".examples"
        if not os.path.isfile(examplesFile):
            print(examplesFile)
            r = input("File with trainExamples not found. Continue? [y|n]")
            if r != "y":
                sys.exit()
        else:
            print("File with trainExamples found. Read it.")
            with open(examplesFile, "rb") as f:
                self.trainExamplesHistory = Unpickler(f).load()
            f.closed
            # examples based on the model were already collected (loaded)
            # self.skipFirstSelfPlay = True
