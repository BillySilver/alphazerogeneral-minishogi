import math
import numpy as np
EPS = 1e-8


class MCTS():
    """
    This class handles the MCTS tree.
    """

    def __init__(self, game, nnet, args):
        self.game = game
        self.nnet = nnet
        self.args = args
        self.Qsa = {}       # stores Q values for s,a (as defined in the paper)
        self.Nsa = {}       # stores #times edge s,a was visited
        self.Ns = {}        # stores #times board s was visited
        self.Ps = {}        # stores initial policy (returned by neural net)

        self.Es = {}        # stores game.getGameEnded ended for board s
        self.Vs = {}        # stores game.getValidMoves for board s

    def clear(self):
        # clear the mcts tree
        self.Qsa.clear()
        self.Nsa.clear()
        self.Ns.clear()
        self.Ps.clear()
        self.Es.clear()
        self.Vs.clear()

    def Dirichlet_root(self, canonicalBoard, alpha, round):
        s = self.game.stringRepresentation(canonicalBoard)

        self.Ps[s], v = self.nnet.predict(canonicalBoard)
        valids = self.game.getValidMoves(canonicalBoard, 1, round=round)

        self.Ps[s] = 0.75 * self.Ps[s] + 0.25 * np.random.dirichlet(alpha * np.ones(self.game.getActionSize()))  # add Dirichlet_noise
        self.Ps[s] = self.Ps[s] * valids      # masking invalid moves
        sum_Ps_s = np.sum(self.Ps[s])

        if sum_Ps_s > 0:
            self.Ps[s] /= sum_Ps_s  # renormalize
        else:
            # if all valid moves were masked make all valid moves equally probable

            # NB! All valid moves may be masked if either your NNet architecture is insufficient or you've get overfitting or something else.
            # If you have got dozens or hundreds of these messages you should pay attention to your NNet and/or training process.
            print("All valid moves were masked, do workaround.")
            self.Ps[s] = self.Ps[s] + valids
            self.Ps[s] /= np.sum(self.Ps[s])

        self.Vs[s] = valids
        self.Ns[s] = (0 if s not in self.Ns else self.Ns[s] + 1)

    def getActionProb(self, canonicalBoard, temp=1, round=0, is_Pit=False):
        """
        This function performs numMCTSSims simulations of MCTS starting from
        canonicalBoard.

        Returns:
            probs: a policy vector where the probability of the ith action is
                   proportional to Nsa[(s,a)]**(1./temp)
        """
        # self.game.display(canonicalBoard,
        #     str_prefix='Input canonicalBoard:\n',
        #     str_suffix='\n%s' % ('-'*20))

        if self.args.Dirichlet:
            self.Dirichlet_root(canonicalBoard, self.args.alpha, round)

        for _ in range(self.args.numMCTSSims):
            self.search(canonicalBoard, depth=0, round=round, is_Pit=is_Pit)

        s = self.game.stringRepresentation(canonicalBoard)
        counts = np.array([self.Nsa[(s, a)] if (s, a) in self.Nsa else 0 for a in range(self.game.getActionSize())])

        if temp == 0:
            bestA = np.argmax(counts)
            probs = np.zeros_like(counts)
            probs[bestA] = 1
            return probs

        counts = counts ** (1./temp)
        probs = counts / counts.sum()
        return probs

    def search(self, canonicalBoard, depth=0, round=0, is_Pit=False):
        """
        This function performs one iteration of MCTS. It is recursively called
        till a leaf node is found. The action chosen at each node is one that
        has the maximum upper confidence bound as in the paper.

        Once a leaf node is found, the neural network is called to return an
        initial policy P and a value V for the state. This value is propogated
        up the search path. In case the leaf node is a terminal state, the
        outcome is propogated up the search path. The values of Ns, Nsa, Qsa are
        updated.

        NOTE: the return values are the negative of the value of the current
        state. This is done since v is in [-1,1] and if v is the value of a
        state for the current player, then its value is -v for the other player.
        Returns:
            v: the negative of the value of the current canonicalBoard
        """

        s = self.game.stringRepresentation(canonicalBoard)

        if s not in self.Es:
            self.Es[s] = self.game.getGameEnded(canonicalBoard, 1)
        if self.Es[s] != 0:
            # terminal node
            # print("Depth =", depth)
            # print("self.Es[s] =", self.Es[s])
            # self.game.display(canonicalBoard,
            #     str_prefix='Terminal board:\n',
            #     str_suffix='\n%s' % ('-'*20))
            return -self.Es[s]

        if s not in self.Ps:
            # Leaf node
            prior_prob, v = self.nnet.predict(canonicalBoard)
            valids = self.game.getValidMoves(canonicalBoard, 1, depth=depth, round=round)
            # print(valids)
            prior_prob = prior_prob * valids      # masking invalid moves
            sum_Ps_s = np.sum(prior_prob)
            if sum_Ps_s > 0:
                prior_prob /= sum_Ps_s    # renormalize
            else:
                # if all valid moves were masked make all valid moves equally probable

                # NB! All valid moves may be masked if either your NNet architecture is insufficient or you've get overfitting or something else.
                # If you have got dozens or hundreds of these messages you should pay attention to your NNet and/or training process.
                print("All valid moves were masked, do workaround.")
                prior_prob = valids
                prior_prob /= np.sum(prior_prob)

            # Initialize only if things for board (s) have been prepared.
            self.Vs[s] = valids
            self.Ns[s] = 0
            self.Ps[s] = prior_prob
            # self.game.display(canonicalBoard,
            #     str_prefix='Leaf node board:\n',
            #     str_suffix='%s' % ('-'*20))
            return -v

        valids = self.Vs[s]
        cur_best = -float('inf')
        best_act = -1

        valid_acts, = valids.nonzero()      # Ignore a huge amount of zeros to accelerate.
        # Pick the action with the highest upper confidence bound
        for a in valid_acts:
            if (s, a) in self.Qsa:
                u = float(self.Qsa[(s, a)] + self.args.cpuct * self.Ps[s][a] * math.sqrt(self.Ns[s]) / (1 + self.Nsa[(s, a)]))
            else:
                # Q = 0 ?
                u = float(self.args.cpuct * self.Ps[s][a] * math.sqrt(self.Ns[s] + EPS))

            if u > cur_best:
                cur_best = u
                best_act = a

        a = best_act

        next_s, next_player = self.game.getNextState(canonicalBoard, 1, a)
        next_s = self.game.getCanonicalForm(next_s, next_player)

        if depth == 200:  # Over 200 moves, definitely repetitions.
            # print("\nLevel =", depth)
            return 0  # return 0 -> Draw, Ns[s] += 1.

        v = self.search(next_s, depth + 1, round, is_Pit)

        if (s, a) in self.Nsa:
            self.Qsa[(s, a)] = (self.Nsa[(s, a)] * self.Qsa[(s, a)] + v) / (self.Nsa[(s, a)] + 1)
            self.Nsa[(s, a)] += 1

        else:
            self.Qsa[(s, a)] = v
            self.Nsa[(s, a)] = 1

        self.Ns[s] += 1
        return -v
