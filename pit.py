import Arena
from Exact_Win_MCTS import MCTS as Exact_Win_MCTS
from MCTS import MCTS
from minishogi.MiniShogiGame import MiniShogiGame, display
from minishogi.MiniShogiPlayers import HumanMiniShogiPlayer
from minishogi.tensorflow.NNet import NNetWrapper as NNet

import os
import numpy as np
from utils import dotdict
import tensorflow as tf

"""
use this script to play any two agents against each other, or play manually with
any agent.
"""

# set gpu
os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Low verbose level of TensorFlow.

# set gpu growth
# config = tf.ConfigProto()
# config.gpu_options.allow_growth = True
# sess = tf.Session(config=config)

g = MiniShogiGame()

# all players
hp = HumanMiniShogiPlayer(g).play


# nnet players
n1 = NNet(g)
n1.load_checkpoint('./Exact_Win_MCTS_models/', 'best_50.pth.tar')
args1 = dotdict({'Dirichlet': False, 'alpha': 0.15, 'numMCTSSims': 800, 'cpuct': 3})
mcts1 = MCTS(g, n1, args1)
n1p = lambda x, round: np.random.choice(g.getActionSize(),
                                        p=mcts1.getActionProb(x, temp=0, round=round))

n2 = NNet(g)
n2.load_checkpoint('./models/', 'best_50.pth.tar')
args2 = dotdict({'Dirichlet': False, 'alpha': 0.15, 'numMCTSSims': 800, 'cpuct': 3})
mcts2 = MCTS(g, n2, args2)
n2p = lambda x, round: np.random.choice(g.getActionSize(),
                                        p=mcts2.getActionProb(x, temp=0, round=round))

arena = Arena.Arena(n1p, n2p, g, display=display)
print(arena.playGames(50, verbose=True))
