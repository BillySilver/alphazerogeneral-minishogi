Profiling and Performance Improvement log
=========================================
Do profilng on `main.py`, which should be specified to use the **serial process** (`Coach.py`) instead of parallel precess (`Coach2.py`).
```bash
python3 -m cProfile -o main.pstats main.py
python3 -m gprof2dot -f pstats main.pstats | dot -T png -o main.profile.png
```

For obtaining results quickly, setting `args.numIters = 3` and `args.numEps = 2` in `main.py` is recommended.

Result
------
Treat the cumulative percentage of time for ***NNet.predict()*** as the performance metric; the higher it is, the faster training process is.

Each *MCTS.search()* calls ***NNet.predict()*** around 3.6-3.8 times. Further, the implementation of ***NNet.predict()*** is basically changeless. Thus whenever the other functions cost less time, the percentage grows. It is suitable to be treated as a metric.

(arranged by change time in descending order)
___
* 46.88% - **Speed up MCTS: editable NNGameBoard** [currently]
* 44.26% - **Speed up MCTS: reusing previous Board** [14040f79]
* 41.51% - **Faster Board.getStrDrops() / Board._move_parser()** [398cdb8a]
* 40.32% - **Faster get_King_Pos() and getObjGameCapture()** [7b2852e7]
* 38.61% - **Replaced np.roll() and np.vectorize()** [11a272a6]
* 37.61% - **Board() needs to set initial values now** [6c14d28f]
* 36.19% - **Replace some Piece.move() @ MiniShogiLogic** [b7d29a0f]
* 36.44% - **Trick to skip redundant in-check test** [3c8562a2]
* 34.67% - **Faster isInCheck() @ MiniShogiLogic** [803dee83]
* 33.26% - **More efficient @ MiniShogiPieces** [713ce41f]
* 30.89% - **Check valid actions only @ MCTS.search()** [e192695b]
* 28.79% - **Remove Piece.setOpposite()** [15012521]
* 27.73% - **Simplify MiniShogiGame.getCanonicalForm()** [3d8d665a]
* 25.60% - **Updating Board.strgameboard on demand** [6b4e64c7]
* 21.86% - **Speed up MCTS: reimplementation @ MiniShogiGame.py** [6e36830b]
* 20.97% - *MiniShogiGame.getObjGameBoard()* : *np.ndarray.nonzero()* instead of *scipy.sparse.\*.nonzero()*
* 13.48% - *MiniShogiGame.getObjGameBoard()* : try scipy.sparse and its *nonzero()*
* 08.51% - **Feature: compress examples in the history** [26c53aa3]

Requirements
------------
If you want to visualize profiling results, the following packages are required.

For Python module `gprof2dot`:
```bash
sudo pip3 install gprof2dot
```
For command `dot` (Debian/Ubuntu):
```bash
sudo apt install graphviz
```
