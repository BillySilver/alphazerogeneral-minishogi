from Arena import Arena
from MCTS import MCTS
from minishogi.MiniShogiGame import MiniShogiGame, display
from minishogi.MiniShogiPlayers import HumanMiniShogiPlayer
from minishogi.tensorflow.NNet import NNetWrapper as NNet

import os
import numpy as np
import tensorflow as tf
import multiprocessing
from utils import dotdict
from pytorch_classification.utils import Bar, AverageMeter

"""
use this script to play any two agents against each other parallelly and get
statistical results.
"""


def async_against(game, args, iter_num, bar):
    """Parallel version competition

    Kindly Note:
    If you set temp = 0 for MCTS.getActionProb(), then the correspounding
    player will use the action with the highest probability, i.e., argmax().
    On the other hand, if temp = 1, then one will pick an action with
    the probability proportional given by MCTS.getActionProb().
    Since we expect that every game will not end with the same result, temp
    should NOT be set as 0. And each worker MUST update its random seed.

    If you desire reproducible results or more MCTS spanning nodes,
    run `pit.py` instead.

    However, the best action policy is still allowed: set args.is_best_action
    as True.

    Arguments:
        game {Game} -- an instance of Game or its inheritor.
        args {dict} -- a dictionary of settings.
        iter_num {int} -- an ordinal number to indicate the order of a job.
        bar {Bar} -- to show a progress bar.

    Returns:
        tuple of 5 ints -- the first four are the number of winnings for
                           player 1 first, player 2 first, player 1 second
                           and player 2 second; and the last is the number of
                           draws.
    """
    if args.different_random_seeds:
        # To prevent workers from getting the same results.
        np.random.seed()
    temp = 0 if args.is_best_action else 1

    n_job = args.numPlayGames // args.numGamesPerJob
    n_finished_job = max(0, iter_num+1 - min(n_job, args.numPlayPool))
    bar.next(n_finished_job)
    bar.suffix = "work:{i}/{x} | Total: {total:} | ETA: {eta:}".format(
        i=iter_num+1, x=n_job, total=bar.elapsed_td, eta=bar.eta_td)
    bar.next(iter_num+1 - n_finished_job)
    if iter_num + 1 == n_job:
        bar.finish()

    # set gpu
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Low verbose level of TensorFlow.

    # set gpu growth
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    sess = tf.Session(config=config)

    # create NN
    model1 = NNet(game)
    model2 = NNet(game)

    # try load weight
    try:
        model1.load_checkpoint(folder=args.model1Folder,
                               filename=args.model1FileName)
    except:
        print("load model1 fail")
        pass
    try:
        model2.load_checkpoint(folder=args.model2Folder,
                               filename=args.model2FileName)
    except:
        print("load model2 fail")
        pass

    # create MCTS
    mcts1 = MCTS(game, model1, args)
    mcts2 = MCTS(game, model2, args)

    # do play 2 games
    n1p = lambda x, round: np.random.choice(game.getActionSize(),
                                            p=mcts1.getActionProb(x, temp=temp, round=round))
    n2p = lambda x, round: np.random.choice(game.getActionSize(),
                                            p=mcts2.getActionProb(x, temp=temp, round=round))
    arena = Arena(n1p, n2p, game, display=display)
    arena.displayBar = False
    if args.numGamesPerJob == 1:
        m1Fwins, m2Fwins, m1Swins, m2Swins, draws = arena.playGame_anyfirst(iter_num % 2, verbose=False)
    else:
        m1Fwins, m2Fwins, m1Swins, m2Swins, draws = arena.playGames(args.numGamesPerJob, verbose=False)
    # bookkeeping
    print('Job', iter_num, ':', m1Fwins, m2Fwins, m1Swins, m2Swins, draws)
    return m1Fwins, m2Fwins, m1Swins, m2Swins, draws


if __name__ == "__main__":
    args = dotdict({
        'numMCTSSims': 800,
        'cpuct': 3.75,

        # 'numPlayGames' means total number of games you wish to play.
        # 'numGamesPerJob' means how many games each job will play (for parallel computing).
        # Each job plays multiple games, thus the total number of games should
        # be a multiple of it.
        'numPlayGames': 16,
        'numGamesPerJob': 1,
        'numPlayPool': 8,

        'model1Folder': './Exact_Win_MCTS_models/',
        'model1FileName': 'best_50.pth.tar',
        'model2Folder': './models/',
        'model2FileName': 'best_50.pth.tar',

        'Dirichlet': False,
        'alpha': 0.15,

        # 'is_best_action' to control if the player will pick the best action.
        # If so, the results should be the same whatever how many jobs are since
        # the parameters of the models are fixed. In general, it should be set
        # as False.
        # Setting 'different_random_seeds' as True leads to games will not end
        # with the same result.
        'is_best_action': False,
        'different_random_seeds': True,
    })

    def parallel_self_test_play():
        assert (args.numGamesPerJob % 2 == 0) or (args.numGamesPerJob == 1)
        assert args.numPlayGames % args.numGamesPerJob == 0
        n_job = args.numPlayGames // args.numGamesPerJob

        g = MiniShogiGame()
        bar = Bar('Verify Play', max=n_job)
        pool = multiprocessing.Pool(processes=args.numPlayPool)
        res = []
        result = []
        for i in range(n_job):
            import time
            time.sleep(0.1)     # Make the progress bar display better.
            res.append(pool.apply_async(async_against, args=(g, args, i, bar)))
        pool.close()
        pool.join()
        bar.finish()

        oneFirstWon = 0
        twoFirstWon = 0
        oneSecondWon = 0
        twoSecondWon = 0
        draws = 0
        for i in res:
            result.append(i.get())
        for i in result:
            oneFirstWon += i[0]
            twoFirstWon += i[1]
            oneSecondWon += i[2]
            twoSecondWon += i[3]
            draws += i[4]
        print()
        print('Model 1 First  Wins: %3d' % oneFirstWon, end='  ')
        print('Model 2 First  Wins: %3d' % twoFirstWon)
        print('Model 1 Second Wins: %3d' % oneSecondWon, end='  ')
        print('Model 2 Second Wins: %3d' % twoSecondWon)
        print('Total:               %3d' % (oneFirstWon + oneSecondWon), end='  ')
        print('                     %3d' % (twoFirstWon + twoSecondWon), end='  ')
        print('Draws:', draws)

    parallel_self_test_play()
