import numpy as np
import time
from minishogi.MiniShogiGame import MiniShogiGame as Game
from minishogi.MiniShogiGameSlow import MiniShogiGameSlow as Slow
from minishogi.MiniShogiPlayer import Player

def speed_test(func, args, str_prompt, n_iter=10000):
    if type(args) is not tuple:
        args = (args, )

    time_beg = time.time()
    for i in range(n_iter):
        tmp = func(*args)
    time_diff = time.time() - time_beg
    print('%35s :' % str_prompt, time_diff)
    return time_diff

# Fairer comparison. It will call faster version for getObjGameBoard(),
# getObjGameCapture(), getNNGameBoard() and getStrGameBoard().
Game._getCanonicalForm_orig = Slow.getCanonicalForm


game = Game()
slow = Slow()
init_board = game.getInitBoard()
init_obj   = game.getObjGameBoard(init_board)
init_str   = game.getStrGameBoard(init_obj)
player0 = Player(1, [])
player1 = Player(1, ['g', 's', 'b', 'r', 'p'])
player2 = Player(-1, ['G', 'S', 'B', 'R', 'P'])


time_diff_a = speed_test((lambda x, y, v: [ [v for j in range(x)] for i in range(y) ]), (5, 5, None), 'list of list of None')
time_diff_b = speed_test(np.empty, ((5, 5), object), 'np.empty of None')
print('-%7.4f%%' % (100 - 100*(time_diff_b / time_diff_a)))
print('-'*35)


time_diff_a = speed_test((lambda x, y, v: [ [v for j in range(x)] for i in range(y) ]), (5, 5, ''), 'list of list of null str')
time_diff_b = speed_test(np.empty, ((5, 5), 'U2'), 'np.empty of null U2')
print('-%7.4f%%' % (100 - 100*(time_diff_b / time_diff_a)))
print('-'*35)


time_diff_a = speed_test(slow.getObjGameBoard, init_board, 'getObjGameBoard_orig()')
time_diff_b = speed_test(slow.getObjGameBoard_proto, init_board, 'getObjGameBoard_proto()')
time_diff_c = speed_test(game.getObjGameBoard, init_board, 'getObjGameBoard()')
print('-%7.4f%%' % (100 - 100*(time_diff_c / time_diff_a)))
print('-'*35)


time_diff_a = speed_test(slow.getStrGameBoard, init_obj, 'getStrGameBoard_orig()')
time_diff_b = speed_test(game.getStrGameBoard, init_obj, 'getStrGameBoard()')
print('-%7.4f%%' % (100 - 100*(time_diff_b / time_diff_a)))
print('-'*35)


board_with_capture = init_board.copy()
time_diff_a = speed_test(slow.getObjGameCapture, (init_board, 1), 'getObjGameCapture_orig() w/ 0ps')
time_diff_b = speed_test(game.getObjGameCapture, (init_board, 1), 'getObjGameCapture() w/ 0ps')
print('-%7.4f%%' % (100 - 100*(time_diff_b / time_diff_a)))
print('-'*35)


board_with_capture = init_board.copy()
board_with_capture[:, :, list(range(20, 30))] = 1
time_diff_a = speed_test(slow.getObjGameCapture, (board_with_capture, 1), 'getObjGameCapture_orig() w/ 10ps')
time_diff_b = speed_test(game.getObjGameCapture, (board_with_capture, 1), 'getObjGameCapture() w/ 10ps')
print('-%7.4f%%' % (100 - 100*(time_diff_b / time_diff_a)))
print('-'*35)


board_with_capture = init_board.copy()
board_with_capture[:, :, list(range(20, 40))] = 1
time_diff_a = speed_test(slow.getObjGameCapture, (board_with_capture, 1), 'getObjGameCapture_orig() w/ 20ps')
time_diff_b = speed_test(game.getObjGameCapture, (board_with_capture, 1), 'getObjGameCapture() w/ 20ps')
print('-%7.4f%%' % (100 - 100*(time_diff_b / time_diff_a)))
print('-'*35)


time_diff_a = speed_test(slow.getNNGameBoard, (init_str, player0, player0), 'getNNGameBoard_orig() w/ 0ps')
time_diff_b = speed_test(game.getNNGameBoard, (init_str, player0, player0), 'getNNGameBoard() w/ 0ps')
print('-%7.4f%%' % (100 - 100*(time_diff_b / time_diff_a)))
print('-'*35)


time_diff_a = speed_test(slow.getNNGameBoard, (init_str, player1, player0), 'getNNGameBoard_orig() w/ 5ps')
time_diff_b = speed_test(game.getNNGameBoard, (init_str, player1, player0), 'getNNGameBoard() w/ 5ps')
print('-%7.4f%%' % (100 - 100*(time_diff_b / time_diff_a)))
print('-'*35)


time_diff_a = speed_test(slow.getNNGameBoard, (init_str, player1, player2), 'getNNGameBoard_orig() w/ 10ps')
time_diff_b = speed_test(game.getNNGameBoard, (init_str, player1, player2), 'getNNGameBoard() w/ 10ps')
print('-%7.4f%%' % (100 - 100*(time_diff_b / time_diff_a)))
print('-'*35)


board_with_capture = init_board.copy()
time_diff_a = speed_test(slow.getCanonicalForm, (board_with_capture, -1), '# getCanonicalForm_orig() w/ 0ps')
time_diff_b = speed_test(game._getCanonicalForm_orig, (board_with_capture, -1), 'getCanonicalForm_orig() w/ 0ps')
time_diff_c = speed_test(game.getCanonicalForm, (board_with_capture, -1), 'getCanonicalForm() w/ 0ps')
print('-%7.4f%%' % (100 - 100*(time_diff_c / time_diff_b)))
print('-'*35)


board_with_capture = init_board.copy()
board_with_capture[:, :, list(range(20, 30))] = 1
time_diff_a = speed_test(slow.getCanonicalForm, (board_with_capture, -1), '# getCanonicalForm_orig() w/ 10ps')
time_diff_b = speed_test(game._getCanonicalForm_orig, (board_with_capture, -1), 'getCanonicalForm_orig() w/ 10ps')
time_diff_c = speed_test(game.getCanonicalForm, (board_with_capture, -1), 'getCanonicalForm() w/ 10ps')
print('-%7.4f%%' % (100 - 100*(time_diff_c / time_diff_b)))
print('-'*35)


board_with_capture = init_board.copy()
board_with_capture[:, :, list(range(20, 40))] = 1
time_diff_a = speed_test(slow.getCanonicalForm, (board_with_capture, -1), '# getCanonicalForm_orig() w/ 20ps')
time_diff_b = speed_test(game._getCanonicalForm_orig, (board_with_capture, -1), 'getCanonicalForm_orig() w/ 20ps')
time_diff_c = speed_test(game.getCanonicalForm, (board_with_capture, -1), 'getCanonicalForm() w/ 20ps')
print('-%7.4f%%' % (100 - 100*(time_diff_c / time_diff_b)))
print('-'*35)
