pit2.py tester
==============

Because reimplementation may ruin the work, we need a tester whose results are reproducible.

Here we adopt the two pretrained models and make them competitive with each other. If randomness is excluded, the executed results should be exactly the same. It doesn't matter that how we implement important methods.

In addition, the execution time can be treated as a performance metric.

How to
------
Run `python3 pit2.py` before checking and setting the followings.

### async_against():
* **mcts1.getActionProb()** / **mcts2.getActionProb()**
    * set argument `temp = 0`
    * lead it to return a one-hot vector to indicate an action which owns the maximum frequency.
    * using `args.is_best_action = True` instead.
* **arena.playGames()**
    * set argument `num = 4`
    * 4 games each worker. Player 1 be first in the first two games.
    * using `args.numGamesPerJob = 4` instead.

### \_\_main\_\_:
Create 2-4 workers and the same amount of jobs to check if the results are **WITHOUT randomness**.
```python
    args.numPlayPool    = [2-4]
    args.numGamesPerJob = 4
    args.numPlayGames   = 4 * args.numPlayPool
    args.Dirichlet      = False
    args.is_best_action = True
```
Here are the pretrained models for bug testing.
```python
    args.model1Folder   = './Exact_Win_MCTS_models/'
    args.model1FileName = 'best_50.pth.tar'
    args.model2Folder   = './models/'
    args.model2FileName = 'best_50.pth.tar'
```
You can also check if the models were changed.
```bash
    md5sum Exact_Win_MCTS_models/* models/*
    e7cc1a502d4302534b6ac9f12e47b424  Exact_Win_MCTS_models/best_50.pth.tar.data-00000-of-00001
    a33e58df4589b123291044f56867347d  Exact_Win_MCTS_models/best_50.pth.tar.index
    127132448ecc79c94990b59451e012cd  Exact_Win_MCTS_models/best_50.pth.tar.meta
    8ede58784c9114f7b34174a0a0911efc  models/best_50.pth.tar.data-00000-of-00001
    cfd44cc68fc9017bbfd24ca23d7a86bd  models/best_50.pth.tar.index
    127132448ecc79c94990b59451e012cd  models/best_50.pth.tar.meta
```

Result
------
The games should end with `26, 76, 49 and 30` rounds sequentially.

Note: if "Checkmate prevention has the highest priority" @ *MiniShogiGame.getValidMoves()* is disabled (from bd38579a until 6e36830b), ones should end with `34, 32, 29 and 14` rounds.

Note: to obtain the exactly same result via `pit.py` (serial computing version of `pit2.py`), set `cpuct = 3.75` for both two players. Otherwise (keep `cpuct = 3`), the game will end with `53, 72, 28, 37` rounds sequentially.

Exe-time: (arranged by change time in descending order)
___
* 0:14:26 (-_4.84%) - **Speed up MCTS: editable NNGameBoard** [currently]
* 0:15:10 (-_8.17%) - **Speed up MCTS: reusing previous Board** [14040f79]
* 0:16:31 (-_2.65%) - **Faster Board.getStrDrops() / Board._move_parser()** [398cdb8a]
* 0:16:58 (-_3.05%) - **Faster get_King_Pos() and getObjGameCapture()** [7b2852e7]
* 0:17:30 (-_3.40%) - **Replaced np.roll() and np.vectorize()** [11a272a6]
* 0:18:07 (-_2.34%) - **Board() needs to set initial values now** [6c14d28f]
* 0:18:33 (-_0.89%) - **Replace some Piece.move() @ MiniShogiLogic** [b7d29a0f]
* 0:18:43 (-_2.60%) - **Trick to skip redundant in-check test** [3c8562a2]
* 0:19:13 (-_4.55%) - **Faster isInCheck() @ MiniShogiLogic** [803dee83]
* 0:20:08 (-_3.44%) - **More efficient @ MiniShogiPieces** [713ce41f]
* 0:20:51 (-_6.85%) - **Check valid actions only @ MCTS.search()** [e192695b]
* 0:22:23 (-_4.07%) - **Remove Piece.setOpposite()** [15012521]
* 0:23:20 (-11.89%) - **Simplify MiniShogiGame.getCanonicalForm()** [3d8d665a]
* 0:26:29 (-37.54%) - **Updating Board.strgameboard on demand** [6b4e64c7]
* 0:42:24 (-66.29%) - **Speed up MCTS: reimplementation @ MiniShogiGame.py** [6e36830b]
* 2:05:46 (-_6.02%) - **Feature: compress examples in the history** [26c53aa3]
* 2:13:49 (_______) - **ORIGINAL WORK** [5c94650d]
