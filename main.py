# from Coach import Coach
from Coach2 import Coach
from minishogi.MiniShogiGame import MiniShogiGame as Game
from minishogi.tensorflow.NNet import NNetWrapper as nn
from utils import dotdict
import os

import tensorflow as tf
import multiprocessing
from minishogi.tensorflow.NNet import NNetWrapper as nn

os.environ["CUDA_VISIBLE_DEVICES"] = "0"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'    # Low verbose level of TensorFlow.

# Set gpu memory grow
# config = tf.ConfigProto()
# config.gpu_options.allow_growth = True
# sess = tf.Session(config=config)

args = dotdict({
    'numIters': 30,
    'numEps': 100,
    'tempThreshold': 10,
    'updateThreshold': 0.6,
    'maxlenOfQueue': 200000,
    'numMCTSSims': 400,
    'arenaCompare': 40,
    'cpuct': 2.5,
    'numSelfPlayPool': 12,

    'checkpoint': './temp/',
    'load_model': False,
    'load_folder_file': ('./temp/', 'best_30.pth.tar'),
    'numItersForTrainExamplesHistory': 20,

    'Dirichlet': True,
    'alpha': 0.15,
    'oneNet': True,
    'logName': 'log.txt',
})

# Custom verbose for profiling.
args['is_custom_verbose'] = True
# Whether compress examples (boards especially).
args['is_compressing'] = True


if __name__ == "__main__":
    g = Game()
    nnet = nn(g)

    if args.load_model:
        nnet.load_checkpoint(
            args.load_folder_file[0], args.load_folder_file[1])

    c = Coach(g, nnet, args)
    if args.load_model:
        print("Load trainExamples from file")
        c.loadTrainExamples()
    c.learn()
