import numpy as np
from pytorch_classification.utils import Bar, AverageMeter
import time

class Arena():
    """
    An Arena class where any 2 agents can be pit against each other.
    """
    def __init__(self, mcts, human_player_func, game, display=None):
        from minishogi.MiniShogiGame import all_valid_move
        self.mcts = mcts
        self.human_player_func = human_player_func
        self.game = game
        self.display = display
        self.action_size = game.getActionSize()
        self.all_valid_move = all_valid_move

    def mcts_player_func(self, x, round, temp=0):
        # temp = 0: argmax{a} P(a | x)
        # temp = 1: a ~ P(a | x)
        return np.random.choice(self.action_size,
                                p=self.mcts.getActionProb(x, temp=temp, round=round))

    def competitions(self):
        while True:
            # 1. Input to select an option.
            while True:
                try:
                    print('[1] You (MCTS)  [2] Foe (Human)  [^D] exit')
                    n_option = 0
                    n_option = input('Which player is first: ').strip()
                    n_option = int(n_option)
                except ValueError:
                    continue
                if not (1 <= n_option <= 2):
                    continue
                break

            # 2. Handle the option.
            is_player1_first = (n_option == 1)
            self.competition_once(is_player1_first)

    def competition_once(self, is_player1_first):
        game = self.game
        players = [self.human_player_func, None, self.mcts_player_func]
        n_current_player = 1 if is_player1_first else -1    # 1: You (MCTS); -1: Foe (Human).
        board = self.game.getInitBoard()
        n_round = 1
        self.display(board,
            str_prefix='\nTurn %d Player %d\n' % (n_round, n_current_player),
            str_suffix='\n')
        try:
            self.searching_until_interrupt(board, n_round=n_round)
        except KeyboardInterrupt:
            self.show_previous_search_times_after_interrupt()

        while True:
            try:
                # 1. Input to select an option.
                while True:
                    try:
                        print('[1] keep searching  [2] make an action  [9] end this game  [^D] exit')
                        n_option = 0
                        n_option = input('Select an option: ').strip()
                        n_option = int(n_option)
                    except ValueError:
                        continue
                    if not (1 <= n_option <= 2 or n_option == 9):
                        continue
                    break

                # 2. Handle the option.
                if n_option == 1:
                    self.searching_until_interrupt(board, n_round=n_round)
                elif n_option == 2:
                    player_func = players[n_current_player+1]

                    # Regardless of player position.
                    canonical_board = game.getCanonicalForm(board, n_current_player)
                    # Chooce an action.
                    n_round_offset = 1 if n_current_player == -1 and not is_player1_first else 0
                    action = player_func(canonical_board, n_round+n_round_offset)
                    n_current_player == 1 and print('* Action:', self.all_valid_move[action])
                    # Get the next state.
                    board, n_current_player = game.getNextState(board, n_current_player, action)

                    n_round += 1
                    self.display(board,
                        str_prefix='\nTurn %d Player %d\n' % (n_round, n_current_player),
                        str_suffix='\n')

                    r = self.game.getGameEnded(board, 1)
                    if r:
                        print('Player %d Won!\n' % r)
                        return

                    # Keep searching when idle.
                    n_option = 1
                    self.searching_until_interrupt(board, n_round=n_round)
                elif n_option == 9:
                    print('Game terminated by user.\n')
                    return

            except KeyboardInterrupt:
                if n_option == 1:
                    self.show_previous_search_times_after_interrupt()

    def searching_until_interrupt(self, board, n_round=0):
        import time
        print('* MCTSearching... (^C to interrupt)')
        self.n_search = 0
        self.search_beg_time = time.time()
        while True:
            self.mcts.search(board, round=n_round)
            self.n_search += 1

    def show_previous_search_times_after_interrupt(self):
        import time
        if self.n_search == 0:
            print()
            return
        sec_diff = time.time() - self.search_beg_time
        sec_per_search = sec_diff / self.n_search
        print('\n* Searched %d times for the current board. %.3f/%.3fs\n' % (self.n_search, sec_diff, sec_per_search))
        self.n_search = 0
